﻿using System;
using System.Configuration;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using FPS.MNTMgr.Services;
using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Services.DataAccess;
using FPS.MNTMgr.Repositories;

namespace FPS.MNTMgr.Test
{
    [TestClass]
    public class DbTests
    {

        ConnectionStringSettings _defaultConnectionStringSettings;
        CommDataAccess _commDataAccess;



        public DbTests()
        {
            _defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["MNTMGRDWH"];
            _commDataAccess = new CommDataAccess(_defaultConnectionStringSettings);

            FPS.MNTMgr.Repositories.SiteRepository.Init();
        }

        [TestMethod]
        public void MtrSiteAreasInsertTest()
        {

            string returnValue = _commDataAccess.MtrSiteAreasInsert("Demo", "Bay-1", "PHOTO Tools", "PHOTO Tools");

            Assert.AreEqual(AppSettings.Message.Success, returnValue);
        }

        [TestMethod]
        public void MtrEquipmentsInsertTest1()
        {

            string returnValue = _commDataAccess.MtrEquipmentsInsert("Demo", "Bay-1", "ASML1S", "ASML1","Stepper","ASML, Stepper", "ASML, Stepper");

            Assert.AreEqual(AppSettings.Message.Success, returnValue);
        }

        [TestMethod]
        public void MtrEquipmentsInsertTest2()
        {

            string returnValue = _commDataAccess.MtrEquipmentsInsert("Demo", "Bay-2", "ASML1S", "ASML1", "Stepper", "ASML, Stepper", "ASML, Stepper");

            Assert.AreEqual(AppSettings.Message.Success, returnValue);
        }

        [TestMethod]
        public void MtrSkillInsertTest()
        {

            string returnValue = _commDataAccess.MtrSkillsInsert("Demo", "Writing", "Writing", "Writing");

            Assert.AreEqual(AppSettings.Message.Success, returnValue);
        }

        [TestMethod]
        public void MtrSkillTruncateTest()
        {

            string returnValue = _commDataAccess.MtrSkillsTruncate();

            Assert.AreEqual(AppSettings.Message.Success, returnValue);
        }
    }
}
