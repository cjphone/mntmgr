-- Function: public.mtr_engineer_skills_insert(character varying, character varying, character varying, real, character varying)

-- DROP FUNCTION public.mtr_engineer_skills_insert(character varying, character varying, character varying, real, character varying);

CREATE OR REPLACE FUNCTION public.mtr_engineer_skills_insert(
    in_site character varying,
    in_username character varying,
    in_skill character varying,
    in_skill_level real,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_engineer_skills (site, username, skill, skill_level, description) 
	values (in_site, in_username, in_skill, in_skill_level, in_description)
	on conflict (site, username, skill)
	do update set (skill_level, description) = (in_skill_level, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_engineer_skills_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_engineer_skills_insert(character varying, character varying, character varying, real, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_engineer_skills_insert(character varying, character varying, character varying, real, character varying) IS 'insert new engineer into mtr_engineer_skills';
