-- Function: public.mtr_event_skills_insert(character varying, character varying, character varying, character varying)

-- DROP FUNCTION public.mtr_event_skills_insert(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_event_skills_insert(
    in_site character varying,
    in_event character varying,
    in_skill character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_event_skills (site, event, skill, description) 
	values (in_site, in_event, in_skill, in_description)
	on conflict (site, event, skill)
	do update set (description) = (in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_event_skills_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_event_skills_insert(character varying, character varying, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_event_skills_insert(character varying, character varying, character varying, character varying) IS 'insert new event into mtr_event_skills';
