-- Function: public.mtr_events_insert(character varying, character varying, real, character varying, character varying)

-- DROP FUNCTION public.mtr_events_insert(character varying, character varying, real, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_events_insert(
    in_site character varying,
    in_event character varying,
    in_required_hours real,
    in_display_name character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_events (site, event, required_hours, display_name, description) 
	values (in_site, in_event, in_required_hours, in_display_name, in_description)
	on conflict (site, event)
	do update set (required_hours, display_name, description) = (in_required_hours, in_display_name, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_events_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_events_insert(character varying, character varying, real, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_events_insert(character varying, character varying, real, character varying, character varying) IS 'insert new equipment into mtr_events';
