-- Function: public.mtr_skills_truncate()

-- DROP FUNCTION public.mtr_skills_truncate();

CREATE OR REPLACE FUNCTION public.mtr_skills_truncate()
  RETURNS character AS
$BODY$
BEGIN 

	TRUNCATE TABLE "mtr_skills";

	RETURN 'Truncated';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_skills_truncate()
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_skills_truncate() IS 'truncate table, mtr_skills';
