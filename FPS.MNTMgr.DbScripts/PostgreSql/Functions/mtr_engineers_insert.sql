-- Function: public.mtr_engineers_insert(character, character, character varying, character varying, character varying)

-- DROP FUNCTION public.mtr_engineers_insert(character, character, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_engineers_insert(
    in_username character,
    in_password character,
    in_full_name character varying,
    in_display_name character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_engineers (username, password, full_name, display_name, description) 
	values (in_username, in_password, in_full_name, in_display_name, in_description)
	on conflict (username)
	do update set (password, full_name, display_name, description) = (in_password, in_full_name, in_display_name, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_engineers_insert(character, character, character varying, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_engineers_insert(character, character, character varying, character varying, character varying) IS 'insert new engineer into mtr_engineers';
