-- Function: public.mtr_site_engineers_insert(character varying, character varying, character varying, character varying)

-- DROP FUNCTION public.mtr_site_engineers_insert(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_site_engineers_insert(
    in_site character varying,
    in_username character varying,
    in_status character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_site_engineers (site, username, status, description) 
	values (in_site, in_username, in_status, in_description)
	on conflict (site, username)
	do update set (status, description) = (in_status, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_site_engineers_insert(character varying, character varying, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_site_engineers_insert(character varying, character varying, character varying, character varying) IS 'insert new equipment into mtr_site_engineers';
