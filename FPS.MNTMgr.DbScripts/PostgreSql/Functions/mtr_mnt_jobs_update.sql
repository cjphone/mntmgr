-- Function: public.mtr_mnt_jobs_update(character varying, character varying, character varying, character varying, date, date, character varying, date, character varying, date, date)

-- DROP FUNCTION public.mtr_mnt_jobs_update(character varying, character varying, character varying, character varying, date, date, character varying, date, character varying, date, date);

CREATE OR REPLACE FUNCTION public.mtr_mnt_jobs_update(
    in_site character varying,
    in_job_id character varying,
    in_display_name character varying,
    in_description character varying,
    in_early_due_inst date,
    in_late_due_inst date,
    in_scheduled_engineer character varying DEFAULT NULL::character varying,
    in_scheduled_start_inst date DEFAULT NULL::date,
    in_actual_engineer character varying DEFAULT NULL::character varying,
    in_actual_start_inst date DEFAULT NULL::date,
    in_actual_end_inst date DEFAULT NULL::date)
  RETURNS character AS
$BODY$
BEGIN 


	update public.mtr_mnt_jobs 
	set
		display_name = in_display_name,
		description = in_description,
		
		early_due_inst = in_early_due_inst, 
		late_due_inst = in_late_due_inst, 

		scheduled_engineer = in_scheduled_engineer, 
		scheduled_start_inst = in_scheduled_start_inst, 

		actual_engineer = in_actual_engineer, 
		actual_start_inst = in_actual_start_inst, 
		actual_end_inst = in_actual_end_inst
	
	where site = in_site
	and job_id = in_job_id;
	
	RETURN 'Updated';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_mnt_jobs_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_mnt_jobs_update(character varying, character varying, character varying, character varying, date, date, character varying, date, character varying, date, date)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_mnt_jobs_update(character varying, character varying, character varying, character varying, date, date, character varying, date, character varying, date, date) IS 'updated existing event in mtr_mnt_jobs';
