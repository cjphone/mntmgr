-- Function: public.mtr_engineer_avail_hours_insert(character varying, character varying, date, real, character varying)

-- DROP FUNCTION public.mtr_engineer_avail_hours_insert(character varying, character varying, date, real, character varying);

CREATE OR REPLACE FUNCTION public.mtr_engineer_avail_hours_insert(
    in_site character varying,
    in_username character varying,
    in_work_day date,
    in_available_hours real,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_engineer_avail_hours (site, username, work_day, available_hours, description) 
	values (in_site, in_username, in_work_day, in_available_hours, in_description)
	on conflict (site, username, work_day)
	do update set (available_hours, description) = (in_available_hours, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_engineer_skills_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE || '[' || in_site || ']');
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_engineer_avail_hours_insert(character varying, character varying, date, real, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_engineer_avail_hours_insert(character varying, character varying, date, real, character varying) IS 'insert new engineer into mtr_engineer_avail_hours';
