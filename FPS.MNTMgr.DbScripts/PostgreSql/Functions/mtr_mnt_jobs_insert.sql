-- Function: public.mtr_mnt_jobs_insert(character varying, character varying, character varying, character varying, character varying, character varying, integer, date, date)

-- DROP FUNCTION public.mtr_mnt_jobs_insert(character varying, character varying, character varying, character varying, character varying, character varying, integer, date, date);

CREATE OR REPLACE FUNCTION public.mtr_mnt_jobs_insert(
    in_site character varying,
    in_job_id character varying,
    in_eqp_id character varying,
    in_event character varying,
    in_display_name character varying,
    in_description character varying,
    in_job_priority integer,
    in_early_due_inst date,
    in_late_due_inst date)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_mnt_jobs (site, job_id, eqp_id, event, display_name, description, job_priority, early_due_inst, late_due_inst) 
	values (in_site, in_job_id, in_eqp_id, in_event, in_display_name, in_description, in_job_priority, in_early_due_inst, in_late_due_inst)
	on conflict (site, job_id, eqp_id, event, late_due_inst)
	do update set (display_name, description, job_priority, early_due_inst, late_due_inst) = 
	(in_display_name, in_description, in_job_priority, in_early_due_inst, in_late_due_inst);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_mnt_jobs_insert', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_mnt_jobs_insert(character varying, character varying, character varying, character varying, character varying, character varying, integer, date, date)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_mnt_jobs_insert(character varying, character varying, character varying, character varying, character varying, character varying, integer, date, date) IS 'insert new event into mtr_mnt_jobs';
