-- Function: public.mtr_site_areas_insert(character, character, character varying, character varying)

-- DROP FUNCTION public.mtr_site_areas_insert(character, character, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_site_areas_insert(
    in_site character,
    in_area character,
    in_display_name character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_site_areas (site, area, display_name, description) 
	values (in_site, in_area, in_display_name, in_description)
	on conflict (site, area)
	do update set (display_name, description) = (in_display_name, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_site_areas_insert(character, character, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_site_areas_insert(character, character, character varying, character varying) IS 'insert new equipment into mtr_site_areas';
