-- Function: public.mtr_engineer_avail_hours_delete(character varying, character varying, date)

-- DROP FUNCTION public.mtr_engineer_avail_hours_delete(character varying, character varying, date);

CREATE OR REPLACE FUNCTION public.mtr_engineer_avail_hours_delete(
    in_site character varying,
    in_username character varying,
    in_work_day date)
  RETURNS character AS
$BODY$
BEGIN 


	delete from public.mtr_engineer_avail_hours
	where site = in_site
	and username = in_username
	and work_day = in_work_day;

	RETURN 'Deleted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;

	insert into public.mtr_logs(site, log_type, log_action, log_message)
	values(in_site, 'Error', 'mtr_engineer_avail_hours_delete', 'Failed:' || SQLERRM || ',' || SQLSTATE);
	
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_engineer_avail_hours_delete(character varying, character varying, date)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_engineer_avail_hours_delete(character varying, character varying, date) IS 'Deleted working hours on mtr_engineer_avail_hours';
