-- Function: public.mtr_equipments_insert(character, character, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION public.mtr_equipments_insert(character, character, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_equipments_insert(
    in_site character,
    in_area character,
    in_eqp_id character varying,
    in_main_eqp_id character varying,
    in_eqp_type character varying,
    in_display_name character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_equipments (site, area, eqp_id, main_eqp_id, eqp_type, display_name, description) 
	values (in_site, in_area, in_eqp_id, in_main_eqp_id, in_eqp_type, in_display_name, in_description)
	on conflict (site, eqp_id)
	do update set (area, main_eqp_id, eqp_type, display_name, description) = (in_area, in_main_eqp_id, in_eqp_type, in_display_name, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_equipments_insert(character, character, character varying, character varying, character varying, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_equipments_insert(character, character, character varying, character varying, character varying, character varying, character varying) IS 'insert new equipment into mtr_equipments';
