-- Table: public.mtr_skills

-- DROP TABLE public.mtr_skills;

CREATE TABLE public.mtr_skills
(
  site character varying(16) NOT NULL,
  skill character varying(64) NOT NULL,
  display_name character varying(64) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_skills_pk PRIMARY KEY (site, skill),
  CONSTRAINT mtr_skills_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_skills
  OWNER TO mntmgr;
