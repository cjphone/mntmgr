-- Table: public.mtr_mnt_jobs

-- DROP TABLE public.mtr_mnt_jobs;

CREATE TABLE public.mtr_mnt_jobs
(
  site character varying(16) NOT NULL,
  job_id character varying(128) NOT NULL,
  eqp_id character varying(64) NOT NULL,
  event character varying(64) NOT NULL,
  display_name character varying(64) NOT NULL,
  job_priority integer NOT NULL DEFAULT 1,
  early_due_inst date NOT NULL DEFAULT ('now'::text)::date,
  late_due_inst date NOT NULL DEFAULT ('now'::text)::date,
  scheduled_engineer character varying(128),
  scheduled_start_inst date,
  actual_engineer character varying(128),
  actual_start_inst date,
  actual_end_inst date,
  description character varying(256),
  CONSTRAINT mtr_mnt_jobs_pk PRIMARY KEY (site, job_id, eqp_id, event, late_due_inst),
  CONSTRAINT mtr_mnt_jobs_fk_actual_engineer FOREIGN KEY (actual_engineer)
      REFERENCES public.mtr_engineers (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_mnt_jobs_fk_eqp_id FOREIGN KEY (site, eqp_id)
      REFERENCES public.mtr_equipments (site, eqp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_mnt_jobs_fk_event FOREIGN KEY (site, event)
      REFERENCES public.mtr_events (site, event) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_mnt_jobs_fk_scheduled_engineer FOREIGN KEY (scheduled_engineer)
      REFERENCES public.mtr_engineers (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_mnt_jobs
  OWNER TO mntmgr;
