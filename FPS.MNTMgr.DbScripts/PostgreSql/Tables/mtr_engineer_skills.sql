-- Table: public.mtr_engineer_skills

-- DROP TABLE public.mtr_engineer_skills;

CREATE TABLE public.mtr_engineer_skills
(
  site character varying(16) NOT NULL,
  username character varying(128) NOT NULL,
  skill character varying(64) NOT NULL,
  skill_level numeric(4,1) NOT NULL DEFAULT 10, -- 10 is best, ...
  description character varying(256),
  CONSTRAINT mtr_engineer_skills_pk PRIMARY KEY (site, username, skill),
  CONSTRAINT mtr_engineer_skills_fk_skill FOREIGN KEY (site, skill)
      REFERENCES public.mtr_skills (site, skill) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_engineer_skills_fk_username FOREIGN KEY (username)
      REFERENCES public.mtr_engineers (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_engineer_skills_chk_skill_level CHECK (skill_level >= 1::numeric AND skill_level <= 10::numeric) -- check skill level....
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_engineer_skills
  OWNER TO mntmgr;
COMMENT ON COLUMN public.mtr_engineer_skills.skill_level IS '10 is best, 
0, is worst';

COMMENT ON CONSTRAINT mtr_engineer_skills_chk_skill_level ON public.mtr_engineer_skills IS 'check skill level.
it will only allow 0 to 10';

