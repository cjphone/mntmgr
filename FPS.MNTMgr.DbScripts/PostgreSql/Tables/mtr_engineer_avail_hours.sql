-- Table: public.mtr_engineer_avail_hours

-- DROP TABLE public.mtr_engineer_avail_hours;

CREATE TABLE public.mtr_engineer_avail_hours
(
  site character varying(16) NOT NULL,
  username character varying(128) NOT NULL,
  work_day date NOT NULL,
  available_hours numeric(4,1) NOT NULL DEFAULT 8, -- total available hours
  description character varying(256),
  CONSTRAINT mtr_engineer_avail_hours_pk PRIMARY KEY (site, username, work_day),
  CONSTRAINT mtr_engineer_avail_hours_fk_username FOREIGN KEY (username)
      REFERENCES public.mtr_engineers (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_engineer_avail_hours_chk_available_hours CHECK (available_hours >= 0::numeric AND available_hours <= 24::numeric) -- check available hours. it only allows 0 to 24
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_engineer_avail_hours
  OWNER TO mntmgr;
COMMENT ON COLUMN public.mtr_engineer_avail_hours.available_hours IS 'total available hours';

COMMENT ON CONSTRAINT mtr_engineer_avail_hours_chk_available_hours ON public.mtr_engineer_avail_hours IS 'check available hours. it only allows 0 to 24';

