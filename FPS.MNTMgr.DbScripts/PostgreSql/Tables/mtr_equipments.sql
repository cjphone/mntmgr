-- Table: public.mtr_equipments

-- DROP TABLE public.mtr_equipments;

CREATE TABLE public.mtr_equipments
(
  site character varying(16) NOT NULL,
  area character varying(16) NOT NULL,
  eqp_id character varying(64) NOT NULL,
  main_eqp_id character varying(64) NOT NULL,
  eqp_type character varying(64) NOT NULL,
  display_name character varying(64) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_equipments_pk PRIMARY KEY (site, eqp_id),
  CONSTRAINT mtr_equipments_fk_area FOREIGN KEY (site, area)
      REFERENCES public.mtr_site_areas (site, area) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_equipments
  OWNER TO mntmgr;
