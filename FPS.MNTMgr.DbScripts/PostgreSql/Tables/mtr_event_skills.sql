-- Table: public.mtr_event_skills

-- DROP TABLE public.mtr_event_skills;

CREATE TABLE public.mtr_event_skills
(
  site character varying(16) NOT NULL,
  event character varying(128) NOT NULL,
  skill character varying(64) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_event_skills_pk PRIMARY KEY (site, event, skill),
  CONSTRAINT mtr_event_skills_fk_event FOREIGN KEY (site, event)
      REFERENCES public.mtr_events (site, event) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_event_skills_fk_skill FOREIGN KEY (site, skill)
      REFERENCES public.mtr_skills (site, skill) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_event_skills
  OWNER TO mntmgr;
