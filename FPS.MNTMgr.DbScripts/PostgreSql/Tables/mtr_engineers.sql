-- Table: public.mtr_engineers

-- DROP TABLE public.mtr_engineers;

CREATE TABLE public.mtr_engineers
(
  username character varying(128) NOT NULL,
  password character varying(128) NOT NULL,
  full_name character varying(64) NOT NULL,
  display_name character varying(64) NOT NULL,
  email_address character varying(128),
  phone_number character varying(128),
  description character varying(256),
  CONSTRAINT mtr_engineers_pk PRIMARY KEY (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_engineers
  OWNER TO mntmgr;
