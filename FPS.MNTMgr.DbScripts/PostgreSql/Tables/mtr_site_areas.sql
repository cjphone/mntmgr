-- Table: public.mtr_site_areas

-- DROP TABLE public.mtr_site_areas;

CREATE TABLE public.mtr_site_areas
(
  site character varying(16) NOT NULL,
  area character varying(16) NOT NULL,
  display_name character varying(64) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_site_areas_pk PRIMARY KEY (site, area),
  CONSTRAINT mtr_site_areas_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_site_areas
  OWNER TO mntmgr;
