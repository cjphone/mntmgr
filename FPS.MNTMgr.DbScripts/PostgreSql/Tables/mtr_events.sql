-- Table: public.mtr_events

-- DROP TABLE public.mtr_events;

CREATE TABLE public.mtr_events
(
  site character varying(16) NOT NULL,
  event character varying(64) NOT NULL,
  required_hours numeric(4,1) NOT NULL,
  display_name character varying(64) NOT NULL,
  description character varying(256) NOT NULL,
  CONSTRAINT mtr_events_pk PRIMARY KEY (site, event),
  CONSTRAINT mtr_events_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_events
  OWNER TO mntmgr;
