-- Table: public.mtr_logs

-- DROP TABLE public.mtr_logs;

CREATE TABLE public.mtr_logs
(
  site character varying(16) NOT NULL,
  log_type character varying(64) NOT NULL,
  log_action character varying(1024),
  log_message character varying(1024),
  log_time date NOT NULL DEFAULT ('now'::text)::date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_logs
  OWNER TO mntmgr;
