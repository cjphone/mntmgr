-- Table: public.mtr_site_engineers

-- DROP TABLE public.mtr_site_engineers;

CREATE TABLE public.mtr_site_engineers
(
  site character varying(16) NOT NULL,
  username character varying(128) NOT NULL,
  status character varying(64) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_site_engineers_pk PRIMARY KEY (site, username),
  CONSTRAINT mtr_site_engineers_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT mtr_site_engineers_fk_username FOREIGN KEY (username)
      REFERENCES public.mtr_engineers (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_site_engineers
  OWNER TO mntmgr;
