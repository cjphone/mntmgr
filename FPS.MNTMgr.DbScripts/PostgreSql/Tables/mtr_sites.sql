-- Table: public.mtr_sites

-- DROP TABLE public.mtr_sites;

CREATE TABLE public.mtr_sites
(
  site character varying(16) NOT NULL,
  display_name character varying(64) NOT NULL,
  description character varying(256),
  dashboard_url character varying(128),
  CONSTRAINT mtr_sites_pk PRIMARY KEY (site)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_sites
  OWNER TO mntmgr;
