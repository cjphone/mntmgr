﻿-- Database: "MntMgr"

-- DROP DATABASE "MntMgr";

-- Need to create user login account MntMgr first

CREATE DATABASE "MntMgr"
  WITH OWNER = MntMgr
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;