-- View: public.mtr_p_sites

-- DROP VIEW public.mtr_p_sites;

CREATE OR REPLACE VIEW public.mtr_p_sites AS 
 SELECT mtr_sites.site,
    mtr_sites.display_name,
    mtr_sites.description,
    mtr_sites.dashboard_url
   FROM mtr_sites;

ALTER TABLE public.mtr_p_sites
  OWNER TO mntmgr;
