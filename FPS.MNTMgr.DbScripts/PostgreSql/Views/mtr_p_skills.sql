-- View: public.mtr_p_skills

-- DROP VIEW public.mtr_p_skills;

CREATE OR REPLACE VIEW public.mtr_p_skills AS 
 SELECT mtr_skills.site,
    mtr_skills.skill,
    mtr_skills.display_name,
    mtr_skills.description
   FROM mtr_skills;

ALTER TABLE public.mtr_p_skills
  OWNER TO mntmgr;
