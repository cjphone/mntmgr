-- View: public.mtr_p_site_areas

-- DROP VIEW public.mtr_p_site_areas;

CREATE OR REPLACE VIEW public.mtr_p_site_areas AS 
 SELECT mtr_site_areas.site,
    mtr_site_areas.area,
    mtr_site_areas.display_name,
    mtr_site_areas.description
   FROM mtr_site_areas;

ALTER TABLE public.mtr_p_site_areas
  OWNER TO mntmgr;
