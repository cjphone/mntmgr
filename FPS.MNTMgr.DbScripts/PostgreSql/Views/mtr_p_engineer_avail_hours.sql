-- View: public.mtr_p_engineer_avail_hours

-- DROP VIEW public.mtr_p_engineer_avail_hours;

CREATE OR REPLACE VIEW public.mtr_p_engineer_avail_hours AS 
 SELECT mtr_engineer_avail_hours.site,
    mtr_engineer_avail_hours.username,
    mtr_engineer_avail_hours.work_day,
    mtr_engineer_avail_hours.available_hours,
    mtr_engineer_avail_hours.description
   FROM mtr_engineer_avail_hours;

ALTER TABLE public.mtr_p_engineer_avail_hours
  OWNER TO mntmgr;
