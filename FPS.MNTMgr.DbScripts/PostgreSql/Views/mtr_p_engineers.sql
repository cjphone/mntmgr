-- View: public.mtr_p_engineers

-- DROP VIEW public.mtr_p_engineers;

CREATE OR REPLACE VIEW public.mtr_p_engineers AS 
 SELECT mtr_engineers.username,
    mtr_engineers.password,
    mtr_engineers.full_name,
    mtr_engineers.display_name,
    mtr_engineers.email_address,
    mtr_engineers.phone_number,
    mtr_engineers.description
   FROM mtr_engineers;

ALTER TABLE public.mtr_p_engineers
  OWNER TO mntmgr;
