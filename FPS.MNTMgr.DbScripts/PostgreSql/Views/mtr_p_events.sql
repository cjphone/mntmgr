-- View: public.mtr_p_events

-- DROP VIEW public.mtr_p_events;

CREATE OR REPLACE VIEW public.mtr_p_events AS 
 SELECT mtr_events.site,
    mtr_events.event,
    mtr_events.required_hours,
    mtr_events.display_name,
    mtr_events.description
   FROM mtr_events;

ALTER TABLE public.mtr_p_events
  OWNER TO mntmgr;
