-- View: public.mtr_p_equipments

-- DROP VIEW public.mtr_p_equipments;

CREATE OR REPLACE VIEW public.mtr_p_equipments AS 
 SELECT mtr_equipments.site,
    mtr_equipments.area,
    mtr_equipments.eqp_id,
    mtr_equipments.main_eqp_id,
    mtr_equipments.eqp_type,
    mtr_equipments.display_name,
    mtr_equipments.description
   FROM mtr_equipments;

ALTER TABLE public.mtr_p_equipments
  OWNER TO mntmgr;
