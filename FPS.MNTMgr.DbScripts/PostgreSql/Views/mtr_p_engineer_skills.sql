-- View: public.mtr_p_engineer_skills

-- DROP VIEW public.mtr_p_engineer_skills;

CREATE OR REPLACE VIEW public.mtr_p_engineer_skills AS 
 SELECT mtr_engineer_skills.site,
    mtr_engineer_skills.username,
    mtr_engineer_skills.skill,
    mtr_engineer_skills.skill_level,
    mtr_engineer_skills.description
   FROM mtr_engineer_skills;

ALTER TABLE public.mtr_p_engineer_skills
  OWNER TO mntmgr;
