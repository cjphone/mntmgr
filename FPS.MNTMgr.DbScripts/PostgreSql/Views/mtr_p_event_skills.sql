-- View: public.mtr_p_event_skills

-- DROP VIEW public.mtr_p_event_skills;

CREATE OR REPLACE VIEW public.mtr_p_event_skills AS 
 SELECT mtr_event_skills.site,
    mtr_event_skills.event,
    mtr_event_skills.skill,
    mtr_event_skills.description
   FROM mtr_event_skills;

ALTER TABLE public.mtr_p_event_skills
  OWNER TO mntmgr;
