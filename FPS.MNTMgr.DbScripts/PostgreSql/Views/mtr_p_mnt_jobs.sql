-- View: public.mtr_p_mnt_jobs

-- DROP VIEW public.mtr_p_mnt_jobs;

CREATE OR REPLACE VIEW public.mtr_p_mnt_jobs AS 
 SELECT mtr_mnt_jobs.site,
    mtr_mnt_jobs.job_id,
    mtr_mnt_jobs.eqp_id,
    mtr_mnt_jobs.event,
    mtr_mnt_jobs.display_name,
    mtr_mnt_jobs.job_priority,
    mtr_mnt_jobs.early_due_inst,
    mtr_mnt_jobs.late_due_inst,
    mtr_mnt_jobs.scheduled_engineer,
    mtr_mnt_jobs.scheduled_start_inst,
    mtr_mnt_jobs.actual_engineer,
    mtr_mnt_jobs.actual_start_inst,
    mtr_mnt_jobs.actual_end_inst,
    mtr_mnt_jobs.description
   FROM mtr_mnt_jobs;

ALTER TABLE public.mtr_p_mnt_jobs
  OWNER TO mntmgr;
