-- View: public.mtr_p_site_engineers

-- DROP VIEW public.mtr_p_site_engineers;

CREATE OR REPLACE VIEW public.mtr_p_site_engineers AS 
 SELECT mtr_site_engineers.site,
    mtr_site_engineers.username,
    mtr_site_engineers.description,
    mtr_site_engineers.status
   FROM mtr_site_engineers;

ALTER TABLE public.mtr_p_site_engineers
  OWNER TO mntmgr;
