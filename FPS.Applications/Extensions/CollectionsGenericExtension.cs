﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FPS.Applications.Extensions
{
    public static class CollectionsGenericExtension
    {
        public static List<List<T>> GetCombos<T>(this List<T> list, int minCount, int maxCount)
        {
            List<List<T>> returnLists = new List<List<T>>();


            try
            {
                if (list != null && list.Count > 0)
                {
                    List<T> initList = new List<T>();
                    returnLists.Add(initList);

                    foreach (T t in list)
                    {
                        List<List<T>> existingLists = returnLists.ToList();

                        foreach (List<T> existingList in existingLists)
                        {
                            List<T> newList = existingList.ToList();
                            newList.Add(t);
                            returnLists.Add(newList);
                        }
                    }
                }

                returnLists = returnLists.Where(x => x.Count >= minCount && x.Count <= maxCount).ToList();
                return returnLists;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("GetCombos->" + ex.Message);

                return returnLists;
            }
        }

        public static List<List<T>> GetCombos<T>(this List<T> list)
        {
            return list.GetCombos(1, 100);
        }



        public static string Serialize<T>(this List<T> list)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Serialize(list.Distinct().ToList()).ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Serialize->" + ex.Message);

                return ex.Message;
            }
        }

        public static List<T> Deserialize<T>(this string input)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Deserialize<List<T>>(input);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Deserialize->" + ex.Message);

                return new List<T>();
            }
        }

        public static T TryFind<T>(this Dictionary<string, T> dict, string keyId)
        {
            lock (dict)
            {
                if (keyId != null && dict.HasData() && dict.ContainsKey(keyId))
                    return dict[keyId];
            }

            return default(T);
        }

        public static T Find<T>(this ConcurrentDictionary<string, T> dict, string keyId)
        {
            if (keyId != null && dict.HasData() && dict.ContainsKey(keyId))
                return dict[keyId];

            return default(T);
        }

        public static void TryAdd<V>(this IList<V> list, V value)
        {
            lock (list)
            {
                if (!list.Contains(value))
                {
                    list.Add(value);
                }
            }
        }

        public static void TryAdd<K, V>(this Dictionary<K, V> dict, K key, V value)
        {
            lock (dict)
            {
                if (!dict.ContainsKey(key))
                {
                    dict.Add(key, value);
                }
            }
        }

        public static void TryAdd<K, V>(this SortedList<K, V> sortedList, K key, V value)
        {
            lock (sortedList)
            {
                if (!sortedList.ContainsKey(key))
                {
                    sortedList.Add(key, value);
                }
            }
        }

        public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> valueSelector)
        {
            return new ConcurrentDictionary<TKey, TValue>(source.ToDictionary(valueSelector));
        }

        public static bool HasData<T>(this IEnumerable<T> pool)
        {
            return pool != null && pool.Any();
        }

        public static bool HasSameElements<T>(this IEnumerable<T> source, IEnumerable<T> input)
        {
            if (source.HasData() && input.HasData())
            {
                var sourceNotInput = source.Except(input).ToList();
                var inputNotSource = input.Except(source).ToList();

                if (sourceNotInput.HasData() || inputNotSource.HasData())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (source.HasData() && !input.HasData())
            {
                return false;
            }
            else if (!source.HasData() && input.HasData())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool HasSameElements<TKey, TValue>(this IDictionary<TKey, TValue> source, IDictionary<TKey, TValue> input)
        {
            if (source.HasData() && input.HasData())
            {
                var sourceNotInput = source.Except(input).ToList();
                var inputNotSource = input.Except(source).ToList();

                if (sourceNotInput.HasData() || inputNotSource.HasData())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (source.HasData() && !input.HasData())
            {
                return false;
            }
            else if (!source.HasData() && input.HasData())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> valueSelector)
        //{
        //    return new ConcurrentDictionary<TKey, TValue>(source.ToDictionary(valueSelector));
        //}
    }
}
