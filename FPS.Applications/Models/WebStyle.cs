﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.Applications.Models
{
    public class WebStyle
    {
        public string BgColorHtml { get; set; }
        public string TextColorHtml { get; set; }
        public string Font { get; set; }
        public string Border { get; set; }
        public string Margin { get; set; }
        public string Padding { get; set; }
        public string LeftPoint { get; set; }
        public string TopPoint { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Zindex { get; set; }
        public string OtherStyles { get; set; }
        public string WebStyleText
        {
            get
            {
                string temp = string.Empty;
                if (!string.IsNullOrEmpty(BgColorHtml)) temp += "background-color:" + BgColorHtml + "; ";
                if (!string.IsNullOrEmpty(TextColorHtml)) temp += "color:" + TextColorHtml + "; ";
                if (!string.IsNullOrEmpty(Font)) temp += "font:" + Font + "; ";
                if (!string.IsNullOrEmpty(Border)) temp += "border:" + Border + "; ";
                if (!string.IsNullOrEmpty(Margin)) temp += "margin:" + Margin + "; ";
                if (!string.IsNullOrEmpty(Padding)) temp += "padding:" + Padding + "; ";
                if (!string.IsNullOrEmpty(LeftPoint)) temp += "left:" + LeftPoint + "; ";
                if (!string.IsNullOrEmpty(TopPoint)) temp += "top:" + TopPoint + "; ";
                if (!string.IsNullOrEmpty(Width)) temp += "width:" + Width + "; ";
                if (!string.IsNullOrEmpty(Height)) temp += "height:" + Height + "; ";
                if (!string.IsNullOrEmpty(Zindex)) temp += "z-index:" + Zindex + "; ";
                if (!string.IsNullOrEmpty(OtherStyles)) temp += OtherStyles;

                return temp;
            }
        }
    }
}
