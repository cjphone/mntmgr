﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Extensions;

namespace FPS.Applications.Models
{
    /// <summary>
    /// log type
    /// </summary>
    public enum LogType { Log, Error, Warning, Information };

    /// <summary>
    /// the individual message log
    /// </summary>
    [Serializable]
    public class MessageLog
    {
        public LogType LogType { get; set; }

        public string Text { get; set; }

        public string Detail { get; set; }

        public DateTime StartInst { get; set; }

        public DateTime? EndInst { get; set; }

        public double TimeSec
        {
            get
            {
                if (EndInst == null)
                    return 0;
                else
                    return ((DateTime)EndInst).Subtract(StartInst).TotalSeconds;
            }
        }
    }

    /// <summary>
    /// the message logs model
    /// this is a generic model to handle either error message, log message or warning message
    /// </summary>
    public class MessageLogs
    {
        public bool IsLoading { get; set; }

        public MessageLogs()
        {
            logs = new List<MessageLog>();
            IsLoading = false;
        }

        private List<MessageLog> logs;
        public List<MessageLog> Logs { get { return logs; } }

        /// <summary>
        /// the first message on this model
        /// </summary>
        public MessageLog First
        {
            get
            {
                return logs.FirstOrDefault();
            }
        }

        /// <summary>
        /// add new message, usually will be either warning message or error message, or just regular information
        /// </summary>
        /// <param name="text"></param>
        /// <param name="logType"></param>
        /// <param name="detail"></param>
        public void Add(string text, LogType logType = LogType.Information, string detail = null)
        {
            MessageLog message = new MessageLog()
            {
                LogType = logType,
                Text = text,
                Detail = detail,
                StartInst = DateTime.Now,
                EndInst = null
            };

            logs.TryAdd(message);
        }

        public void AddRange(List<MessageLog> messageLogs)
        {
            lock (logs)
            {
                if (messageLogs.HasData())
                {
                    logs.AddRange(messageLogs.Distinct().ToList());
                }
            }
        }

        /// <summary>
        /// this is to start to log the action
        /// </summary>
        /// <param name="text"></param>
        /// <param name="detail"></param>
        public void Start(string text, string detail = null)
        {
            MessageLog message = new MessageLog()
            {
                LogType = LogType.Log,
                Text = text,
                Detail = detail,
                StartInst = DateTime.Now,
                EndInst = null
            };

            logs.TryAdd(message);

        }

        /// <summary>
        /// this is to end a log
        /// </summary>
        /// <param name="text"></param>
        public void End(string text)
        {
            lock (logs)
            {
                MessageLog message = logs.Where(x => x.Text == text && x.EndInst == null).OrderBy(x => x.StartInst).FirstOrDefault();

                if (message == null)
                {
                    message = new MessageLog()
                    {
                        LogType = LogType.Log,
                        Text = text,
                        Detail = text,
                        StartInst = DateTime.Now,
                        EndInst = DateTime.Now
                    };

                    logs.TryAdd(message);
                }
                else
                {
                    message.EndInst = DateTime.Now;
                }
            }
        }


        public double GetTimeSec(string text)
        {
            MessageLog log = logs.FirstOrDefault(x => x.Text == text);
            if (log != null)
            {
                return Math.Round(log.TimeSec, 4);
            }
            else
            {
                return 0;
            }
        }

        public TimeSpan GetTimeSpan(string text)
        {
            MessageLog log = logs.FirstOrDefault(x => x.Text == text);
            if (log != null)
            {
                return TimeSpan.FromSeconds(log.TimeSec);
            }
            else
            {
                return TimeSpan.Zero;
            }
        }
    }
}
