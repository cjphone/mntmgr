﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using FPS.Applications.Models;


namespace FPS.Applications.Libraries
{
    public static class TaskLibrary
    {
        public static class Message
        {
            public static string Success = "SUCCESS";
            public static string Fail = "FAIL";
            public static string Slow = "SLOW";
        }

        public delegate void Func();
        public static void NewTask(List<Task> tasks, Func f, MessageLogs messageLogs, string message)
        {
            Task task;
            TaskFactory factory = new TaskFactory();

            task = factory.StartNew(() =>
            {
                messageLogs.Start(message);
                f();
                messageLogs.End(message);
            });

            tasks.Add(task);
        }

        public static void NewTask(List<Task> tasks, Func f)
        {
            Task task;
            TaskFactory factory = new TaskFactory();

            task = factory.StartNew(() => { f(); });

            tasks.Add(task);
        }

        public static string NewTask(Func f, double timeoutSec)
        {
            //timeoutSec = 60;

            string msg = Message.Success;

            #region with threading cancellation

            bool isTimeOut = false;
            ManualResetEvent wait = new ManualResetEvent(false);
            Thread work = new Thread(new ThreadStart(() =>
            {
                //set the function
                f();

                wait.Set();
            }));

            try
            {
                //start to run
                work.Start();

                //setup 
                Boolean signal = wait.WaitOne((int)timeoutSec * 1000);

                //after x secs if didn't receive the done signal then should stop the work
                if (!signal)
                {
                    work.Abort();
                    isTimeOut = true;
                }

                if (isTimeOut)
                    msg = Message.Slow;
            }
            catch (Exception ex)
            {
                work.Abort();
                msg = Message.Fail;

                System.Diagnostics.Debug.WriteLine(ex);
            }


            return msg;

            #endregion

        }
    }
}
