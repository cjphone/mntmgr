﻿using System;
using System.Web.WebPages;

using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Extensions;
using FPS.Applications.Libraries;

using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Models;
using FPS.MNTMgr.Repositories;

namespace FPS.MNTMgr.Services.DataAccess
{
    /// <summary>
    /// this common service is going to handle everything not related to the sched group
    /// </summary>
    public class CommDataAccess
    {
        private FPS.Applications.Models.MessageLogs messageLogs;

        //data source
        private ConnectionStringSettings defaultConnectionStringSettings;

        private readonly CommDataAccessDbRead dbRead;
        private readonly CommDataAccessDbWrite dbWrite;

        public CommDataAccess(ConnectionStringSettings connectionStringSettings)
            : this(new CommDataAccessDbRead(connectionStringSettings), new CommDataAccessDbWrite(connectionStringSettings))
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }

        private CommDataAccess(CommDataAccessDbRead csDbRead, CommDataAccessDbWrite csDbWrite)
        {
            dbRead = csDbRead;
            dbWrite = csDbWrite;
            messageLogs = new FPS.Applications.Models.MessageLogs();
        }

        public void LogMessage(string logAction, string logMsg)
        {
            //dbWrite.LogMessage(logAction, logMsg);
        }

        /// <summary>
        /// pre loading all data related to the site into the memory
        /// </summary>
        public void LoadData()
        {
            string message = AppSettings.Message.Success;

            try
            {
                Dictionary<string, Site> sites = getSites();

                //need to make sure it has all sites data and base data for each site
                if (sites.HasData())
                {
                    SiteRepository.Sites = sites;
                    SiteRepository.MtrEngineers = getMtrEngineers();

                    //start to load the rest of data

                    //this is loading the skills for each site
                    dbRead.LoadMtrSkills(SiteRepository.Sites);

                    //this is loading engineers for each site and sites for each engineer
                    dbRead.LoadMtrSiteEngineers(SiteRepository.Sites, SiteRepository.MtrEngineers);

                    //this is loading skills for each engineer with skill_level
                    dbRead.LoadMtrEngineerSkills(SiteRepository.Sites, SiteRepository.MtrEngineers);

                    //this is loading skills for each engineer with skill_level
                    dbRead.LoadMtrEngineerAvailHours(SiteRepository.Sites, SiteRepository.MtrEngineers);

                    //this is loading areas for each site
                    dbRead.LoadSiteAreas(SiteRepository.Sites);

                    //this is loading equipments for each site
                    dbRead.LoadMtrEquipments(SiteRepository.Sites);

                    //this is loading the events for each site

                    dbRead.LoadMtrEvents(SiteRepository.Sites);

                    //this is loading required skil for each skill
                    dbRead.LoadMtrEventSkills(SiteRepository.Sites);

                    //this is loading all of required mnt Jobs
                    dbRead.LoadMtrMntJobs(SiteRepository.Sites);

                }
                else
                {
                    message = AppSettings.Message.Error + " : Sites Data having problems";
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("LoadSites->" + ex.Message);

                message = AppSettings.Message.Error + " : " + ex.Message;
            }

            LogMessage("LoadSites", message);

        }

        public void LoadSummaryViewData()
        {
            string message = AppSettings.Message.Success;

            try
            {
                Dictionary<string, Bay> bays = getBays();
                Dictionary<string, ProcessFamily> processFamilies = getProcessFamilies();

                //need to make sure it has all sites data and base data for each site
                if (bays.HasData())
                {
                    //this is loading tools
                    dbRead.LoadTools(bays, processFamilies);

                    //after loading tools, then loading chambers
                    dbRead.LoadToolChambers(bays);

                    //after loading tools, then loading ports
                    dbRead.LoadToolPorts(bays);

                    //switch 
                    SummaryViewRepository.Bays = bays;
                    SummaryViewRepository.ProcessFamilies = processFamilies;

                }
                else
                {
                    message = AppSettings.Message.Error + " : Bay View Data having problems";
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("LoadBayViewData->" + ex.Message);

                message = AppSettings.Message.Error + " : " + ex.Message;
            }

            LogMessage("LoadBayViewData", message);

        }

        /// <summary>
        /// get the sites info
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, Site> getSites()
        {
            Dictionary<string, Site> sites = dbRead.GetSites();

            //loadSiteDatas(sites);

            return sites;

        }

        private Dictionary<string, Bay> getBays()
        {
            Dictionary<string, Bay> bays = dbRead.GetBays();

            //loadSiteDatas(sites);

            return bays;

        }

        private Dictionary<string, ProcessFamily> getProcessFamilies()
        {
            Dictionary<string, ProcessFamily> processFamilies = dbRead.GetProcessFamilies();

            //loadSiteDatas(sites);

            return processFamilies;

        }

        /// <summary>
        /// get the list of engineers, 
        /// rememeber, one engineer can work for different sites
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, MtrEngineer> getMtrEngineers()
        {
            Dictionary<string, MtrEngineer> mtrEngineers = dbRead.GetMtrEngineers();

            //loadSiteDatas(sites);

            return mtrEngineers;

        }

















        /// <summary>
        /// insert new area
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="siteAreaId"></param>
        /// <param name="siteAreaDisplayName"></param>
        /// <param name="siteAreaDescription"></param>
        /// <returns></returns>
        public string MtrSiteAreasInsert(string siteId, string siteAreaId, string siteAreaDisplayName, string siteAreaDescription)
        {
            if (SiteRepository.Sites.ContainsKey(siteId))
            {
                SiteArea siteArea = SiteRepository.Sites[siteId].SiteAreas.Values.FirstOrDefault(x => x.Id == siteAreaId);
                if (siteArea != null)
                {
                    siteArea.DisplayName = siteAreaDisplayName;
                    siteArea.Description = siteAreaDescription;
                }
                else
                {
                    siteArea = new SiteArea(SiteRepository.Sites[siteId], siteAreaId, siteAreaDisplayName) { Description = siteAreaDescription };
                    SiteRepository.Sites[siteId].SiteAreas.TryAdd(siteArea.Id, siteArea);
                }

                return dbWrite.MtrSiteAreasInsert(siteId, siteAreaId, siteAreaDisplayName, siteAreaDescription);
            }
            else
            {
                return "Insufficient data. Please verify";
            }
        }

        /// <summary>
        /// insert engineer working schedule for each site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <param name="workDay"></param>
        /// <param name="availableHours"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrEngineerAvailHoursInsert(string siteId, string userId, DateTime workDay, double availableHours, string description)
        {
            MtrEngineer mtrEngineer = SiteRepository.MtrEngineers.Values.FirstOrDefault(x => x.Id == userId);

            if (mtrEngineer != null)
            {
                if (!mtrEngineer.AvailableHours.ContainsKey(siteId))
                {
                    mtrEngineer.AvailableHours.TryAdd(siteId, new Dictionary<DateTime, double>());
                }
                mtrEngineer.AvailableHours[siteId].TryAdd(workDay, availableHours);
            }

            return dbWrite.MtrEngineerAvailHoursInsert(siteId, userId, workDay, availableHours, description);
        }

        /// <summary>
        /// insert engineer's skill at each site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <param name="skill"></param>
        /// <param name="skillLevel"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrEngineerSkillsInsert(string siteId, string userId, string skill, double skillLevel, string description)
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == siteId);
            MtrSkill mtrSkill = site.MtrSkills.Values.FirstOrDefault(x => x.Id == skill);
            MtrEngineer mtrEngineer = SiteRepository.MtrEngineers.Values.FirstOrDefault(x => x.Id == userId);

            if (site != null && mtrSkill != null && mtrEngineer != null)
            {
                MtrEngineerSkill mtrEngineerSkill = new MtrEngineerSkill(site, mtrEngineer, mtrSkill, skillLevel) { Description = description };
                mtrEngineer.MtrEngineerSkills.TryAdd(mtrEngineerSkill.MtrSkill.Id, mtrEngineerSkill);
            }

            return dbWrite.MtrEngineerSkillsInsert(siteId, userId, skill, skillLevel, description);
        }

        /// <summary>
        /// insert new engineer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrEngineersInsert(string userId, string fullName, string displayName, string description)
        {
            MtrEngineer mtrEngineer = SiteRepository.MtrEngineers.Values.FirstOrDefault(x => x.Id == userId);
            if (mtrEngineer != null)
            {
                mtrEngineer.FullName = fullName;
                mtrEngineer.DisplayName = displayName;
                mtrEngineer.Description = description;
            }
            else
            {
                mtrEngineer = new MtrEngineer(userId, displayName, fullName) { Description = description };
                SiteRepository.MtrEngineers.TryAdd(mtrEngineer.Id, mtrEngineer);
            }
            return dbWrite.MtrEngineersInsert(userId, userId, fullName, displayName, description);
        }

        /// <summary>
        /// insert the engineer to easch site if he is eligible working there
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrSiteEngineersInsert(string siteId, string userId, string status, string description)
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == siteId);
            MtrEngineer mtrEngineer = SiteRepository.MtrEngineers.Values.FirstOrDefault(x => x.Id == userId);

            if (site != null && mtrEngineer != null)
            {
                site.MtrEngineers.TryAdd(mtrEngineer.Id, mtrEngineer);
                mtrEngineer.Sites.TryAdd(site.Id, site);
            }

            return dbWrite.MtrSiteEngineersInsert(siteId, userId, status, description);
        }

        /// <summary>
        /// insert the new eqp
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="EqpArea"></param>
        /// <param name="eqpId"></param>
        /// <param name="mainEqpId"></param>
        /// <param name="eqpType"></param>
        /// <param name="eqpDisplayName"></param>
        /// <param name="eqpDescription"></param>
        /// <returns></returns>
        public string MtrEquipmentsInsert(string siteId, string EqpArea, string eqpId, string mainEqpId, string eqpType, string eqpDisplayName, string eqpDescription)
        {
            if (SiteRepository.Sites.ContainsKey(siteId) && SiteRepository.Sites[siteId].SiteAreas.ContainsKey(EqpArea))
            {
                MtrEquipment mtrEquipment = SiteRepository.Sites[siteId].MtrEquipments.Values.FirstOrDefault(x => x.Id == eqpId);
                if (mtrEquipment != null)
                {
                    if (mtrEquipment.Area != null && mtrEquipment.Area.Id != EqpArea)
                    {
                        mtrEquipment.Area.MtrEquipments.Remove(eqpId);
                        mtrEquipment.Area = SiteRepository.Sites[siteId].SiteAreas[EqpArea];
                        SiteRepository.Sites[siteId].SiteAreas[EqpArea].MtrEquipments.TryAdd(mtrEquipment.Id, mtrEquipment);

                    }

                    mtrEquipment.Type = eqpType;
                    mtrEquipment.MainEquipmentId = mainEqpId;
                    mtrEquipment.DisplayName = eqpDisplayName;
                    mtrEquipment.Description = eqpDescription;
                }
                else
                {
                    mtrEquipment = new MtrEquipment(SiteRepository.Sites[siteId].SiteAreas[EqpArea], eqpId, eqpDisplayName) { Type = eqpType, MainEquipmentId = mainEqpId, DisplayName = eqpDisplayName, Description = eqpDescription };
                    SiteRepository.Sites[siteId].SiteAreas[EqpArea].MtrEquipments.TryAdd(mtrEquipment.Id, mtrEquipment);
                }

                return dbWrite.MtrEquipmentsInsert(siteId, EqpArea, eqpId, mainEqpId, eqpType, eqpDisplayName, eqpDescription);
            }
            else
            {
                return "Insufficient data. Please verify";
            }


        }

        /// <summary>
        /// insert event
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="eventId"></param>
        /// <param name="skill"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrEventSkillsInsert(string siteId, string eventId, string skill, string description)
        {
            if (SiteRepository.Sites.ContainsKey(siteId) && SiteRepository.Sites[siteId].MtrEvents.ContainsKey(eventId) && SiteRepository.Sites[siteId].MtrSkills.ContainsKey(skill))
            {
                MtrEvent mtrEvent = SiteRepository.Sites[siteId].MtrEvents[eventId];
                MtrSkill mtrSkill = SiteRepository.Sites[siteId].MtrSkills[skill];

                mtrEvent.RequiredMtrSkills.TryAdd(mtrSkill.Id, mtrSkill);

                return dbWrite.MtrEventSkillsInsert(siteId, eventId, skill, description);
            }
            else
            {
                return "Insufficient data. Please verify";
            }


        }

        /// <summary>
        /// insert new evernt
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="eventId"></param>
        /// <param name="requiredHours"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrEventsInsert(string siteId, string eventId, string requiredHours, string displayName, string description)
        {

            if (SiteRepository.Sites.ContainsKey(siteId))
            {
                MtrEvent mtrEvent = SiteRepository.Sites[siteId].MtrEvents.Values.FirstOrDefault(x => x.Id == eventId);
                if (mtrEvent != null)
                {
                    mtrEvent.RequiredMntHours = double.Parse(requiredHours);
                    mtrEvent.DisplayName = displayName;
                    mtrEvent.Description = description;
                }
                else
                {
                    mtrEvent = new MtrEvent(SiteRepository.Sites[siteId], eventId, displayName, double.Parse(requiredHours)) { Description = description };
                    SiteRepository.Sites[siteId].MtrEvents.TryAdd(mtrEvent.Id, mtrEvent);
                }

                return dbWrite.MtrEventsInsert(siteId, eventId, mtrEvent.RequiredMntHours, displayName, description);
            }
            else
            {
                return "Insufficient data. Please verify";
            }
        }

        /// <summary>
        /// insert the new mng job
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="jobId"></param>
        /// <param name="eqpId"></param>
        /// <param name="eventId"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <param name="earlyDueInst"></param>
        /// <param name="lateDueInst"></param>
        /// <returns></returns>
        public string MtrMntJobsInsert(string siteId, string jobId, string eqpId, string eventId, string displayName, string description, string earlyDueInst, string lateDueInst)
        {


            bool toSave = true;
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);

            if (site != null)
            {

                DateTime mntEarlyDueDate = DateTime.Now.Date;
                DateTime mntLateDueDate = DateTime.Now.Date;
                MtrEquipment mtrEquipment = null;
                MtrEvent mtrEvent = null;

                if (string.IsNullOrEmpty(jobId))
                {
                    toSave = false;
                }

                if (string.IsNullOrEmpty(displayName))
                {
                    displayName = eventId + " on " + eqpId;
                }

                if (string.IsNullOrEmpty(description))
                {
                    description = eventId + " on " + eqpId;
                }

                if (!string.IsNullOrEmpty(earlyDueInst) && earlyDueInst.IsDateTime())
                {
                    mntEarlyDueDate = DateTime.Parse(earlyDueInst);
                }
                else
                {
                    toSave = false;
                }

                if (!string.IsNullOrEmpty(lateDueInst) && lateDueInst.IsDateTime())
                {
                    mntLateDueDate = DateTime.Parse(lateDueInst);
                }
                else
                {
                    toSave = false;
                }

                if (!string.IsNullOrEmpty(eqpId) && site.MtrEquipments.ContainsKey(eqpId))
                {
                    mtrEquipment = site.MtrEquipments[eqpId];
                }
                else
                {
                    toSave = false;
                }

                if (!string.IsNullOrEmpty(eventId) && site.MtrEvents.ContainsKey(eventId))
                {
                    mtrEvent = site.MtrEvents[eventId];
                }
                else
                {
                    toSave = false;
                }

                if (toSave)
                {
                    try
                    {
                        MtrMntJob mntJob = new MtrMntJob(jobId, mtrEvent, mtrEquipment) { DisplayName = displayName, Description = description };

                        if (mntLateDueDate < mntEarlyDueDate)
                        {
                            mntEarlyDueDate = mntLateDueDate;
                        }

                        mntJob.EarlyDueMntStartInst = mntEarlyDueDate;
                        mntJob.LateDueMntStartInst = mntLateDueDate;
                        mntJob.CompileEligibleMtrEngineers(SiteRepository.MtrEngineers.Values.ToList());
                        mtrEquipment.RequiredMntJobs.Add(mntJob);

                        mntJob.Reset(site.MtrEngineers.Values.ToList());

                        return dbWrite.MtrMntJobsInsert(siteId, jobId, eqpId, eventId, displayName, description, 1, mntEarlyDueDate, mntLateDueDate);

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                }
            }

            return "Insufficient data. Please verify";
        }

        /// <summary>
        /// to be able to update the mnt job scheduled
        /// there will be 3 scenarir
        /// 1, not scheduled, then it should only change early/late due
        /// 2, scheduled,
        /// 3, started 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="jobId"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <param name="earlyDueInst"></param>
        /// <param name="lateDueInst"></param>
        /// <param name="scheduledStartInst"></param>
        /// <param name="actualStartInst"></param>
        /// <param name="actualEndInst"></param>
        /// <returns></returns>
        public string MtrMntJobsUpdate(string siteId, string jobId, string displayName, string description, DateTime earlyDueInst, DateTime lateDueInst, string scheduledEngineerId, DateTime? scheduledStartInst, string actualEngineerId, DateTime? actualStartInst, DateTime? actualEndInst)
        {

            if (SiteRepository.Sites.ContainsKey(siteId) && SiteRepository.Sites[siteId].MtrMntJobs.Any(x => x.Id == jobId))
            {
                MtrMntJob mtrNMtJob = SiteRepository.Sites[siteId].MtrMntJobs.FirstOrDefault(x => x.Id == jobId);



                if (mtrNMtJob.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished)
                {
                    try
                    {


                        return dbWrite.MtrMntJobsUpdate(siteId, jobId, displayName, description, earlyDueInst, lateDueInst, scheduledEngineerId, scheduledStartInst, actualEngineerId, actualStartInst, actualEndInst);

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                }
            }

            return "Insufficient data. Please verify";
        }

        /// <summary>
        /// insert a new skill
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string MtrSkillsInsert(string siteId, string skillId, string displayName, string description)
        {
            if (SiteRepository.Sites.ContainsKey(siteId))
            {
                MtrSkill mtrSkill = SiteRepository.Sites[siteId].MtrSkills.Values.FirstOrDefault(x => x.Id == skillId);
                if (mtrSkill != null)
                {
                    mtrSkill.DisplayName = displayName;
                    mtrSkill.Description = description;
                }
                else
                {
                    mtrSkill = new MtrSkill(SiteRepository.Sites[siteId], skillId, displayName) { Description = description };
                    SiteRepository.Sites[siteId].MtrSkills.TryAdd(mtrSkill.Id, mtrSkill);
                }

                return dbWrite.MtrSkillsInsert(siteId, skillId, displayName, description);
            }
            else
            {
                return "Insufficient data. Please verify";
            }
        }

        /// <summary>
        /// truncate mnt_skills table
        /// </summary>
        /// <returns></returns>
        public string MtrSkillsTruncate()
        {
            return dbWrite.MtrSkillsTruncate();
        }
    }
}
