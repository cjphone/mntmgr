﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Npgsql;

using FPS.MNTMgr.Settings;

namespace FPS.MNTMgr.Services.DataAccess
{
    public class CommDataAccessDbWrite
    {
        private static readonly log4net.ILog fileLogger = log4net.LogManager.GetLogger("FileLogger");

        private ConnectionStringSettings defaultConnectionStringSettings;

        /// <summary>
        /// initialize a new instance for scheduler db data service by using connection string settings
        /// </summary>
        /// <param name="connectionStringSettings"></param>
        internal CommDataAccessDbWrite(ConnectionStringSettings connectionStringSettings)
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }


        /// <summary>
        /// create a new area
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="area"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrSiteAreasInsert(string siteId, string areaId, string displayName, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(areaId) || string.IsNullOrEmpty(displayName) || string.IsNullOrEmpty(description))
            {
                return "Insufficient data.Please verify input data.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_site_areas_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_area", NpgsqlTypes.NpgsqlDbType.Text, areaId);
                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new schedule for engineer
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <param name="workDay"></param>
        /// <param name="availableHours"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEngineerAvailHoursInsert(string siteId, string userId, DateTime workDay, double availableHours, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(userId))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_engineer_avail_hours_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_username", NpgsqlTypes.NpgsqlDbType.Text, userId);
                            cmd.Parameters.AddWithValue("in_work_day", NpgsqlTypes.NpgsqlDbType.Date, workDay);
                            cmd.Parameters.AddWithValue("in_available_hours", NpgsqlTypes.NpgsqlDbType.Real, availableHours);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new skill for engineer
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <param name="skill"></param>
        /// <param name="skillLevel"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEngineerSkillsInsert(string siteId, string userId, string skill, double skillLevel, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(skill))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_engineer_skills_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_username", NpgsqlTypes.NpgsqlDbType.Text, userId);
                            cmd.Parameters.AddWithValue("in_skill", NpgsqlTypes.NpgsqlDbType.Text, skill);
                            cmd.Parameters.AddWithValue("in_skill_level", NpgsqlTypes.NpgsqlDbType.Real, skillLevel);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new engineer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="fullName"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEngineersInsert(string userId, string userPassowrd, string fullName, string displayName, string description)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(displayName))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_engineers_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_username", NpgsqlTypes.NpgsqlDbType.Text, userId);
                            cmd.Parameters.AddWithValue("in_password", NpgsqlTypes.NpgsqlDbType.Text, userPassowrd);
                            cmd.Parameters.AddWithValue("in_full_name", NpgsqlTypes.NpgsqlDbType.Text, fullName);
                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        internal string MtrSiteEngineersInsert(string siteId, string userId, string status, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(userId))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_site_engineers_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_username", NpgsqlTypes.NpgsqlDbType.Text, userId);
                            cmd.Parameters.AddWithValue("in_status", NpgsqlTypes.NpgsqlDbType.Text, status);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new equipment
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="areaId"></param>
        /// <param name="eqpId"></param>
        /// <param name="mainEqpId"></param>
        /// <param name="eqpType"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEquipmentsInsert(string siteId, string areaId, string eqpId, string mainEqpId, string eqpType, string displayName, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(areaId) || string.IsNullOrEmpty(eqpId) || string.IsNullOrEmpty(mainEqpId) || string.IsNullOrEmpty(eqpType) || string.IsNullOrEmpty(displayName))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_equipments_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_area", NpgsqlTypes.NpgsqlDbType.Text, areaId);
                            cmd.Parameters.AddWithValue("in_eqp_id", NpgsqlTypes.NpgsqlDbType.Text, eqpId);
                            cmd.Parameters.AddWithValue("in_main_eqp_id", NpgsqlTypes.NpgsqlDbType.Text, mainEqpId);
                            cmd.Parameters.AddWithValue("in_eqp_type", NpgsqlTypes.NpgsqlDbType.Text, eqpType);
                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// add a new required skill to an event
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="eventId"></param>
        /// <param name="skill"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEventSkillsInsert(string siteId, string eventId, string skill, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(eventId) || string.IsNullOrEmpty(skill))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_event_skills_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_event", NpgsqlTypes.NpgsqlDbType.Text, eventId);
                            cmd.Parameters.AddWithValue("in_skill", NpgsqlTypes.NpgsqlDbType.Text, skill);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new event
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="eventId"></param>
        /// <param name="requiredHours"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrEventsInsert(string siteId, string eventId, double requiredHours, string displayName, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(eventId) || string.IsNullOrEmpty(displayName))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_events_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_event", NpgsqlTypes.NpgsqlDbType.Text, eventId);
                            cmd.Parameters.AddWithValue("in_required_hours", NpgsqlTypes.NpgsqlDbType.Real, requiredHours);
                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new mnt Job
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="jobId"></param>
        /// <param name="eqpId"></param>
        /// <param name="eventId"></param>
        /// <param name="jobPriority"></param>
        /// <param name="earlyDueInst"></param>
        /// <param name="lateDueInst"></param>
        /// <param name="scheduledEngineer"></param>
        /// <param name="scheduledStartInst"></param>
        /// <param name="actualEngineer"></param>
        /// <param name="actualStartInst"></param>
        /// <param name="actualEndInst"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrMntJobsUpdate(string siteId, string jobId, string displayName, string description, DateTime earlyDueInst, DateTime lateDueInst, string scheduledEngineerId, DateTime? scheduledStartInst, string actualEngineerId, DateTime? actualStartInst, DateTime? actualEndInst)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(jobId))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_mnt_jobs_update";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_job_id", NpgsqlTypes.NpgsqlDbType.Text, jobId);

                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            cmd.Parameters.AddWithValue("in_early_due_inst", NpgsqlTypes.NpgsqlDbType.Date, earlyDueInst);
                            cmd.Parameters.AddWithValue("in_late_due_inst", NpgsqlTypes.NpgsqlDbType.Date, lateDueInst);

                            if (string.IsNullOrEmpty(scheduledEngineerId))
                            {
                                cmd.Parameters.AddWithValue("in_scheduled_engineer", NpgsqlTypes.NpgsqlDbType.Text, DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("in_scheduled_engineer", NpgsqlTypes.NpgsqlDbType.Text, scheduledEngineerId);
                            }

                            if (scheduledStartInst == null)
                            {
                                cmd.Parameters.AddWithValue("in_scheduled_start_inst", NpgsqlTypes.NpgsqlDbType.Date, DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("in_scheduled_start_inst", NpgsqlTypes.NpgsqlDbType.Date, (DateTime)scheduledStartInst);
                            }

                            if (string.IsNullOrEmpty(actualEngineerId))
                            {
                                cmd.Parameters.AddWithValue("in_actual_engineer", NpgsqlTypes.NpgsqlDbType.Text, DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("in_actual_engineer", NpgsqlTypes.NpgsqlDbType.Text, actualEngineerId);
                            }

                            if (actualStartInst == null)
                            {
                                cmd.Parameters.AddWithValue("in_actual_start_inst", NpgsqlTypes.NpgsqlDbType.Date, DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("in_actual_start_inst", NpgsqlTypes.NpgsqlDbType.Date, (DateTime)actualStartInst);
                            }

                            if (actualEndInst == null)
                            {
                                cmd.Parameters.AddWithValue("in_actual_end_inst", NpgsqlTypes.NpgsqlDbType.Date, DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("in_actual_end_inst", NpgsqlTypes.NpgsqlDbType.Date, (DateTime)actualEndInst);
                            }

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        internal string MtrMntJobsInsert(string siteId, string jobId, string eqpId, string eventId, string displayName, string description, int jobPriority, DateTime earlyDueInst, DateTime lateDueInst)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(jobId) || string.IsNullOrEmpty(eqpId) || string.IsNullOrEmpty(eventId))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_mnt_jobs_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_job_id", NpgsqlTypes.NpgsqlDbType.Text, jobId);
                            cmd.Parameters.AddWithValue("in_eqp_id", NpgsqlTypes.NpgsqlDbType.Text, eqpId);
                            cmd.Parameters.AddWithValue("in_event", NpgsqlTypes.NpgsqlDbType.Text, eventId);

                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            cmd.Parameters.AddWithValue("in_job_priority", NpgsqlTypes.NpgsqlDbType.Integer, jobPriority);
                            cmd.Parameters.AddWithValue("in_early_due_inst", NpgsqlTypes.NpgsqlDbType.Date, earlyDueInst);
                            cmd.Parameters.AddWithValue("in_late_due_inst", NpgsqlTypes.NpgsqlDbType.Date, lateDueInst);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }

        /// <summary>
        /// create a new skill
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="skill"></param>
        /// <param name="displayName"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        internal string MtrSkillsInsert(string siteId, string skill, string displayName, string description)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(skill) || string.IsNullOrEmpty(displayName))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_skills_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", NpgsqlTypes.NpgsqlDbType.Text, siteId);
                            cmd.Parameters.AddWithValue("in_skill", NpgsqlTypes.NpgsqlDbType.Text, skill);
                            cmd.Parameters.AddWithValue("in_display_name", NpgsqlTypes.NpgsqlDbType.Text, displayName);
                            cmd.Parameters.AddWithValue("in_description", NpgsqlTypes.NpgsqlDbType.Text, description);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }

            }

            return AppSettings.Message.Success;
        }



        /// <summary>
        /// truncate all of skills from mtr_skills table
        /// </summary>
        /// <returns></returns>
        internal string MtrSkillsTruncate()
        {

            string sqlStoredProcedure = "public.mtr_skills_truncate";


            //open the connection 
            using (NpgsqlConnection conn = new NpgsqlConnection())
            {

                try
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using PgSql command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlStoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        //run the execution
                        cmd.ExecuteNonQuery();


                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    fileLogger.Error(AppSettings.Message.Error, ex);

                    return AppSettings.Message.Error + ex.Message;
                }
            }


            return AppSettings.Message.Success;
        }
    }
}
