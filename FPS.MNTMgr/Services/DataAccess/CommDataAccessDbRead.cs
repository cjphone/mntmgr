﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Oracle.ManagedDataAccess.Client;
using Npgsql;

using FPS.Applications.Extensions;
using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Models.BaseModels;
using FPS.MNTMgr.Models;


namespace FPS.MNTMgr.Services.DataAccess
{
    public class CommDataAccessDbRead
    {
        private static readonly log4net.ILog fileLogger = log4net.LogManager.GetLogger("FileLogger");

        private ConnectionStringSettings defaultConnectionStringSettings;


        /// <summary>
        /// initialize a new instance for scheduler db data service by using connection string settings
        /// </summary>
        /// <param name="connectionStringSettings"></param>
        internal CommDataAccessDbRead(ConnectionStringSettings connectionStringSettings)
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }

        /// <summary>
        /// build site list and return
        /// </summary>
        /// <returns></returns>
        internal Dictionary<string, Site> GetSites()
        {
            Dictionary<string, Site> items = new Dictionary<string, Site>();
            Site item;

            try
            {
                string sqlSelect = string.Format(@"
                    select distinct
                        site, 
                        display_name
                    from mtr_p_sites
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string id = reader["site"].ToString().Trim();
                                string display_name = reader["display_name"].ToString().Trim();

                                if (!string.IsNullOrEmpty(id))
                                {
                                    item = new Site(id, display_name);



                                    items.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }

            if (!items.HasData())
            {
                items = new Dictionary<string, Site>();
                item = new Site(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);

                items.Add(item.Id, item);
            }
            return items;
        }



        /// <summary>
        /// build the engineer list and return
        /// </summary>
        /// <returns></returns>
        internal Dictionary<string, MtrEngineer> GetMtrEngineers()
        {
            Dictionary<string, MtrEngineer> items = new Dictionary<string, MtrEngineer>();
            MtrEngineer item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        username, 
                        full_name,
                        display_name,
                        description
                    from mtr_p_engineers
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string username = reader["username"].ToString().Trim();
                                string fullName = reader["full_name"].ToString().Trim();
                                string displayName = reader["display_name"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(displayName))
                                {
                                    item = new MtrEngineer(username, displayName, fullName) { Description = description };

                                    items.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }

            return items;
        }

        /// <summary>
        /// binding the site-engineer relationship
        /// </summary>
        /// <param name="sites"></param>
        /// <param name="mtrEngineers"></param>
        internal void LoadMtrSiteEngineers(Dictionary<string, Site> sites, Dictionary<string, MtrEngineer> mtrEngineers)
        {
            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        username,
                        status,
                        description
                    from mtr_p_site_engineers
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string username = reader["username"].ToString().Trim();
                                string status = reader["status"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(username) && sites.ContainsKey(siteId) && mtrEngineers.ContainsKey(username))
                                {
                                    Site site = sites[siteId];
                                    MtrEngineer mtrEngineer = mtrEngineers[username];

                                    site.MtrEngineers.TryAdd(mtrEngineer.Id, mtrEngineer);
                                    mtrEngineer.Sites.TryAdd(site.Id, site);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }



        /// <summary>
        /// loading all of skills 
        /// </summary>
        /// <param name="sites"></param>
        internal void LoadMtrSkills(Dictionary<string, Site> sites)
        {
            MtrSkill item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        skill,
                        display_name,
                        description
                    from mtr_p_skills
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string skill = reader["skill"].ToString().Trim();
                                string displayName = reader["display_name"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(skill) && sites.ContainsKey(siteId) && !sites[siteId].MtrSkills.ContainsKey(skill))
                                {

                                    item = new MtrSkill(sites[siteId], skill, displayName) { Description = description };


                                    sites[siteId].MtrSkills.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }


        }

        /// <summary>
        /// binding the engineer-skill relationship
        /// </summary>
        /// <param name="sites"></param>
        /// <param name="mtrEngineers"></param>
        internal void LoadMtrEngineerSkills(Dictionary<string, Site> sites, Dictionary<string, MtrEngineer> mtrEngineers)
        {
            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        username,
                        skill,
                        skill_level,
                        description
                    from mtr_p_engineer_skills
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string username = reader["username"].ToString().Trim();
                                string skill = reader["skill"].ToString().Trim();
                                string skillLevel = reader["skill_level"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(username) && sites.ContainsKey(siteId) && mtrEngineers.ContainsKey(username) && sites[siteId].MtrSkills.ContainsKey(skill))
                                {
                                    Site site = sites[siteId];
                                    MtrSkill mtrSkill = site.MtrSkills[skill];
                                    MtrEngineer mtrEngineer = mtrEngineers[username];

                                    MtrEngineerSkill mtrEngineerSkill = new MtrEngineerSkill(site, mtrEngineer, mtrSkill, double.Parse(skillLevel)) { Description = description };
                                    mtrEngineer.MtrEngineerSkills.TryAdd(mtrSkill.Id, mtrEngineerSkill);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }

        internal void LoadMtrEngineerAvailHours(Dictionary<string, Site> sites, Dictionary<string, MtrEngineer> mtrEngineers)
        {
            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        username,
                        work_day,
                        available_hours,
                        description
                    from mtr_p_engineer_avail_hours
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string username = reader["username"].ToString().Trim();
                                string workDay = reader["work_day"].ToString().Trim();
                                string availHours = reader["available_hours"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(username) &&
                                    !string.IsNullOrEmpty(workDay) && !string.IsNullOrEmpty(availHours) &&
                                    sites.ContainsKey(siteId) &&
                                    mtrEngineers.ContainsKey(username))
                                {
                                    Site site = sites[siteId];
                                    MtrEngineer mtrEngineer = mtrEngineers[username];

                                    DateTime workDayDateTime = DateTime.Parse(workDay);
                                    workDayDateTime = new DateTime(workDayDateTime.Year, workDayDateTime.Month, workDayDateTime.Day);
                                    double availHoursDouble = double.Parse(availHours);

                                    if (!mtrEngineer.AvailableHours.ContainsKey(siteId))
                                    {
                                        mtrEngineer.AvailableHours.TryAdd(siteId, new Dictionary<DateTime, double>());
                                    }

                                    mtrEngineer.AvailableHours[siteId].TryAdd(workDayDateTime, availHoursDouble);

                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }


        /// <summary>
        /// loading site areas
        /// </summary>
        /// <param name="sites"></param>
        internal void LoadSiteAreas(Dictionary<string, Site> sites)
        {
            SiteArea item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        area,
                        display_name,
                        description
                    from mtr_p_site_areas
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string area = reader["area"].ToString().Trim();
                                string displayName = reader["display_name"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(area) && sites.ContainsKey(siteId) && !sites[siteId].SiteAreas.ContainsKey(area))
                                {

                                    item = new SiteArea(sites[siteId], area, displayName) { Description = description };


                                    sites[siteId].SiteAreas.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }

        /// <summary>
        /// loading all of equipments
        /// </summary>
        /// <param name="sites"></param>
        internal void LoadMtrEquipments(Dictionary<string, Site> sites)
        {
            MtrEquipment item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        area,
                        eqp_id,
                        main_eqp_id,
                        eqp_type,
                        display_name,
                        description
                    from mtr_p_equipments
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string area = reader["area"].ToString().Trim();
                                string eqpId = reader["eqp_id"].ToString().Trim();
                                string mainEqpId = reader["main_eqp_id"].ToString().Trim();
                                string eqpType = reader["eqp_type"].ToString().Trim();
                                string displayName = reader["display_name"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(area) && !string.IsNullOrEmpty(eqpId) && sites.ContainsKey(siteId) && sites[siteId].SiteAreas.ContainsKey(area) && !sites[siteId].MtrEquipments.ContainsKey(eqpId))
                                {

                                    Site site = sites[siteId];
                                    SiteArea siteArea = site.SiteAreas[area];

                                    item = new MtrEquipment(siteArea, eqpId, displayName)
                                    {
                                        Type = eqpType,
                                        Description = description
                                    };


                                    siteArea.MtrEquipments.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }


        /// <summary>
        /// loading all of skills 
        /// </summary>
        /// <param name="sites"></param>
        internal void LoadMtrEvents(Dictionary<string, Site> sites)
        {
            MtrEvent item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        event,
                        required_hours,
                        display_name,
                        description
                    from mtr_p_events
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string eventId = reader["event"].ToString().Trim();
                                string requiredHours = reader["required_hours"].ToString().Trim();
                                string displayName = reader["display_name"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(eventId) && sites.ContainsKey(siteId) && !sites[siteId].MtrEvents.ContainsKey(eventId))
                                {

                                    item = new MtrEvent(sites[siteId], eventId, displayName, double.Parse(requiredHours)) { Description = description };


                                    sites[siteId].MtrEvents.TryAdd(item.Id, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }


        }

        /// <summary>
        /// binding the engineer-skill relationship
        /// </summary>
        /// <param name="sites"></param>
        /// <param name="mtrEngineers"></param>
        internal void LoadMtrEventSkills(Dictionary<string, Site> sites)
        {
            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site, 
                        event,
                        skill,
                        description
                    from mtr_p_event_skills
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string eventId = reader["event"].ToString().Trim();
                                string skill = reader["skill"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(eventId) && !string.IsNullOrEmpty(skill) &&
                                    sites.ContainsKey(siteId) &&
                                    sites[siteId].MtrEvents.ContainsKey(eventId) &&
                                    sites[siteId].MtrSkills.ContainsKey(skill))
                                {
                                    Site site = sites[siteId];
                                    MtrSkill mtrSkill = site.MtrSkills[skill];
                                    MtrEvent mtrEvent = site.MtrEvents[eventId];

                                    mtrEvent.RequiredMtrSkills.TryAdd(mtrSkill.Id, mtrSkill);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }

        /// <summary>
        /// binding all of mnt jobs
        /// </summary>
        /// <param name="sites"></param>
        internal void LoadMtrMntJobs(Dictionary<string, Site> sites)
        {
            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        site,
                        job_id,
                        eqp_id,
                        event,
                        display_name,
                        job_priority,
                        early_due_inst,
                        late_due_inst,
                        scheduled_engineer,
                        scheduled_start_inst,
                        actual_engineer,
                        actual_start_inst,
                        actual_end_inst,
                        description
                    from mtr_p_mnt_jobs
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string eventId = reader["event"].ToString().Trim();
                                string eqpId = reader["eqp_id"].ToString().Trim();

                                string jobId = reader["job_id"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(eventId) && !string.IsNullOrEmpty(eqpId) &&
                                    sites.ContainsKey(siteId) &&
                                    sites[siteId].MtrEvents.ContainsKey(eventId) &&
                                    sites[siteId].MtrEquipments.ContainsKey(eqpId))
                                {
                                    Site site = sites[siteId];
                                    MtrEquipment mtrEquipment = site.MtrEquipments[eqpId];
                                    MtrEvent mtrEvent = site.MtrEvents[eventId];

                                    MtrMntJob mntJob = new MtrMntJob(jobId, mtrEvent, mtrEquipment)
                                    {
                                        EarlyDueMntStartInst = DateTime.Parse(reader["early_due_inst"].ToString().Trim()),
                                        LateDueMntStartInst = DateTime.Parse(reader["late_due_inst"].ToString().Trim()),
                                        DisplayName = reader["display_name"].ToString().Trim(),
                                        Description = reader["description"].ToString().Trim(),
                                        MntPriority = int.Parse(reader["job_priority"].ToString().Trim())

                                    };

                                    mntJob.Reset(site.MtrEngineers.Values.ToList());

                                    mtrEquipment.RequiredMntJobs.TryAdd(mntJob);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }
        }

        internal Dictionary<string, Bay> GetBays()
        {
            Dictionary<string, Bay> items = new Dictionary<string, Bay>();
            Bay item;

            try
            {
                string sqlSelect = string.Format(@"
                    select bay, facility, building 
                    from fpsapp.mtr_p_bays
                    
                ");

                //open the connection and get the data
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        cmd.BindByName = true;


                        using (IDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                string bayId = reader["bay"].ToString().Trim();
                                if (!string.IsNullOrEmpty(bayId))
                                {
                                    item = new Bay(bayId, bayId);
                                    items.TryAdd(bayId, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }

            if (!items.HasData())
            {
                items = new Dictionary<string, Bay>();
                item = new Bay(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);

                items.Add(item.Id, item);
            }
            return items;
        }

        internal Dictionary<string, ProcessFamily> GetProcessFamilies()
        {
            Dictionary<string, ProcessFamily> items = new Dictionary<string, ProcessFamily>();
            ProcessFamily item;

            try
            {
                string sqlSelect = string.Format(@"
                    select process_family, facility
                    from fpsapp.mtr_p_process_families
                    
                ");

                //open the connection and get the data
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        cmd.BindByName = true;


                        using (IDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                string processFamilyId = reader["process_family"].ToString().Trim();
                                if (!string.IsNullOrEmpty(processFamilyId))
                                {
                                    item = new ProcessFamily(processFamilyId, processFamilyId);
                                    items.TryAdd(processFamilyId, item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }

            if (!items.HasData())
            {
                items = new Dictionary<string, ProcessFamily>();
                item = new ProcessFamily(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);

                items.Add(item.Id, item);
            }
            return items;
        }

        internal void LoadTools(Dictionary<string, Bay> bays, Dictionary<string, ProcessFamily> processFamilies)
        {
            Tool item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        bay, tool, tool_type, process_family, availability,
                        reserve_capacity, reserve_sec,
                        eqp_state,
                        eqp_state_display,
                        eqp_state_chg_inst,
                        bg_color_html,
                        text_color_html
                    from fpsapp.mtr_p_entities
                    where entity = tool
                    and availability <> 'NST'
                    
                ");

                //open the connection and get the data
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        cmd.BindByName = true;


                        using (IDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                string bayId = reader["bay"].ToString().Trim();
                                string processFamilyId = reader["process_family"].ToString().Trim();
                                string toolId = reader["tool"].ToString().Trim();


                                if (!string.IsNullOrEmpty(bayId) && !string.IsNullOrEmpty(toolId) && bays.ContainsKey(bayId) && !bays[bayId].Tools.ContainsKey(toolId))
                                {
                                    item = new Tool(bays[bayId], toolId, toolId)
                                    {
                                        State = reader["eqp_state"].ToString().Trim(),
                                        StateDisplay = reader["eqp_state_display"].ToString().Trim(),
                                        StateChangeInst = DateTime.Parse(reader["eqp_state_chg_inst"].ToString().Trim()),

                                        ReserveCapacity = int.Parse(reader["reserve_capacity"].ToString().Trim()),
                                        ReserveSec = int.Parse(reader["reserve_sec"].ToString().Trim()),

                                        ToolType = reader["tool_type"].ToString().Trim(),
                                        Availability = reader["availability"].ToString().Trim(),
                                    };

                                    bays[bayId].Tools.TryAdd(toolId, item);

                                    item.WebStyle = new Applications.Models.WebStyle()
                                    {
                                        BgColorHtml = reader["bg_color_html"].ToString().Trim(),
                                        TextColorHtml = reader["text_color_html"].ToString().Trim()
                                    };

                                    //for process family
                                    if (!string.IsNullOrEmpty(processFamilyId) && !string.IsNullOrEmpty(toolId) && processFamilies.ContainsKey(processFamilyId) && !processFamilies[processFamilyId].Tools.ContainsKey(toolId))
                                    {
                                        processFamilies[processFamilyId].Tools.TryAdd(toolId, item);
                                    }

                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }


        }

        internal void LoadToolChambers(Dictionary<string, Bay> bays)
        {
            ToolChamber item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        bay, tool, tool_type, process_family, availability,
                        entity, ch, ch_short_display, ch_type,
                        eqp_state,
                        eqp_state_display,
                        eqp_state_chg_inst,
                        bg_color_html,
                        text_color_html
                    from fpsapp.mtr_p_entities
                    where entity <> tool
                    
                ");

                //open the connection and get the data
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        cmd.BindByName = true;


                        using (IDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                string bayId = reader["bay"].ToString().Trim();
                                string toolId = reader["tool"].ToString().Trim();
                                string chamberId = reader["entity"].ToString().Trim();
                                string chamberDisplay = reader["ch_short_display"].ToString().Trim();

                                if (!string.IsNullOrEmpty(bayId) && !string.IsNullOrEmpty(toolId) && !string.IsNullOrEmpty(chamberId) &&
                                    bays.ContainsKey(bayId) && bays[bayId].Tools.ContainsKey(toolId) && !bays[bayId].Tools[toolId].Chambers.ContainsKey(chamberId))
                                {
                                    item = new ToolChamber(bays[bayId].Tools[toolId], chamberId, chamberDisplay)
                                    {
                                        State = reader["eqp_state"].ToString().Trim(),
                                        StateDisplay = reader["eqp_state_display"].ToString().Trim(),
                                        StateChangeInst = DateTime.Parse(reader["eqp_state_chg_inst"].ToString().Trim()),
                                        Availability = reader["availability"].ToString().Trim(),

                                    };

                                    bays[bayId].Tools[toolId].Chambers.TryAdd(chamberId, item);

                                    item.WebStyle = new Applications.Models.WebStyle()
                                    {
                                        BgColorHtml = reader["bg_color_html"].ToString().Trim(),
                                        TextColorHtml = reader["text_color_html"].ToString().Trim()
                                    };
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }


        }

        internal void LoadToolPorts(Dictionary<string, Bay> bays)
        {
            ToolPort item;

            try
            {
                string sqlSelect = string.Format(@"
                    select 
                        bay, tool, port,
                        status,
                        status_display,
                        bg_color_html,
                        text_color_html
                    from fpsapp.mtr_p_ports
                    
                ");

                //open the connection and get the data
                using (OracleConnection conn = new OracleConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (OracleCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        cmd.BindByName = true;


                        using (IDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {

                                string bayId = reader["bay"].ToString().Trim();
                                string toolId = reader["tool"].ToString().Trim();
                                string portId = reader["port"].ToString().Trim();

                                if (!string.IsNullOrEmpty(bayId) && !string.IsNullOrEmpty(toolId) && !string.IsNullOrEmpty(portId) &&
                                    bays.ContainsKey(bayId) && bays[bayId].Tools.ContainsKey(toolId) && !bays[bayId].Tools[toolId].Ports.ContainsKey(portId))
                                {
                                    item = new ToolPort(bays[bayId].Tools[toolId], portId, portId)
                                    {
                                        State = reader["status"].ToString().Trim(),
                                        StateDisplay = reader["status_display"].ToString().Trim(),

                                    };

                                    bays[bayId].Tools[toolId].Ports.TryAdd(portId, item);

                                    item.WebStyle = new Applications.Models.WebStyle()
                                    {
                                        BgColorHtml = reader["bg_color_html"].ToString().Trim(),
                                        TextColorHtml = reader["text_color_html"].ToString().Trim()
                                    };
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(AppSettings.Message.Error, ex);
            }


        }

    }
}
