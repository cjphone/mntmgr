﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.SolverFoundation.Common;
using Microsoft.SolverFoundation.Services;

using FPS.MNTMgr.Models;

namespace FPS.MNTMgr.Services.Schedule
{
    public class MntCalculation
    {
        readonly Site _stie;
        readonly List<MtrMntJob> _mntJobs;
        readonly List<MtrEngineer> _mtrEngineers;
        //private DateTime _schedInst { get; set; }


        private List<MtrDecision> mntDecisions { get; set; }

        public void CompileSchedule(DateTime schedInst)
        {


            List<MtrMntJob> mntJobs = _mntJobs.Where(x =>
                x.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Started &&
                x.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled &&
                x.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished &&
                (
                    x.LateDueMntStartInst <= schedInst ||
                    x.EarlyDueMntStartInst <= schedInst
                )
            ).ToList();

            findSchedule(mntJobs, schedInst);
            saveSchedule(schedInst);
        }

        private void findSchedule(List<MtrMntJob> mntJobs, DateTime schedInst)
        {
            //first to initialize the new context
            SolverContext context = SolverContext.GetContext();

            //this is very important. it needs to clear the model first
            context.ClearModel();

            //carete new the model
            Model model = context.CreateModel();




            //first term
            Term maxJobs = 0;
            Dictionary<string, Term> engineerTerms = new Dictionary<string, Term>();
            Dictionary<string, Term> jobTerms = new Dictionary<string, Term>();

            foreach (MtrMntJob mntJob in mntJobs)
            {
                Term term = 0;
                jobTerms.Add(mntJob.Id, term);
            }

            foreach (MtrEngineer mtrEngineer in _mtrEngineers)
            {
                Term term = 0;
                engineerTerms.Add(mtrEngineer.Id, term);
            }

            mntDecisions = new List<MtrDecision>();

            foreach (MtrMntJob mntJob in mntJobs)
            {
                foreach (MtrEngineer mtrEngineer in mntJob.EligibleMtrEngineers)
                {

                    MtrDecision mntDecision = new MtrDecision(mntJob, mtrEngineer, schedInst);

                    mntDecisions.Add(mntDecision);

                    mntDecision.Decision = new Decision(Domain.IntegerRange(0, 1), mntDecision.Id) { Description = mntDecision.Id };
                    model.AddDecisions(mntDecision.Decision);

                    if (engineerTerms.ContainsKey(mtrEngineer.Id))
                    {
                        engineerTerms[mtrEngineer.Id] += mntDecision.Decision * mntJob.MtrEvent.RequiredMntHours;
                    }

                    if (jobTerms.ContainsKey(mntJob.Id))
                    {
                        jobTerms[mntJob.Id] += mntDecision.Decision * 1;
                    }

                    maxJobs += mntDecision.Decision * mntDecision.Score;
                }
            }

            foreach (MtrMntJob mntJob in mntJobs)
            {
                model.AddConstraints("job_" + mntJob.Id + "_limits", 0 <= jobTerms[mntJob.Id] <= 1);
            }

            foreach (MtrEngineer mtrEngineer in _mtrEngineers.Where(x => x.AvailableHours.ContainsKey(_stie.Id)))
            {
                double availableHours = 0;
                if (mtrEngineer.AvailableHours[_stie.Id].ContainsKey(schedInst))
                {
                    availableHours = mtrEngineer.AvailableHours[_stie.Id][schedInst];
                }

                if (availableHours > 0 && mtrEngineer.AssignedMntJobs.ContainsKey(schedInst))
                {
                    availableHours -= mtrEngineer.AssignedMntJobs[schedInst].Sum(x => x.MtrEvent.RequiredMntHours);
                }

                //to determine how many hours left for the current date
                if (availableHours > 0 && DateTime.Now.Date == schedInst.Date && _stie.MntScheduleTimes.ContainsKey(schedInst) && _stie.MntScheduleTimes[schedInst].StartInst <= DateTime.Now)
                {
                    //assume every date start from 8:00 am to 6:00 pm
                    availableHours += _stie.MntScheduleTimes[schedInst].StartInst.Subtract(DateTime.Now).TotalHours;

                }

                if (availableHours < 0)
                {
                    availableHours = 0;
                }
                model.AddConstraints("engineer_" + mtrEngineer.Id + "_limits", 0 <= engineerTerms[mtrEngineer.Id] <= availableHours);
            }

            model.AddGoal("maxJobs", GoalKind.Maximize, maxJobs);
            Solution solution = context.Solve(new SimplexDirective());
            Report report = solution.GetReport();

        }

        private void saveSchedule(DateTime schedInst)
        {
            foreach (MtrDecision mntDecision in mntDecisions.Where(x => x.Decision.ToString() == "1"))
            {
                mntDecision.MntJob.ScheduledMtrEngineer = mntDecision.MtrEngineer;
                if (!mntDecision.MtrEngineer.AssignedMntJobs.ContainsKey(schedInst))
                {
                    mntDecision.MtrEngineer.AssignedMntJobs.Add(schedInst, new List<MtrMntJob>());
                }

                mntDecision.MtrEngineer.AssignedMntJobs[schedInst].Add(mntDecision.MntJob);

            }

            foreach (MtrEngineer mtrEngineer in _mtrEngineers.Where(x => x.AssignedMntJobs.ContainsKey(schedInst)))
            {
                double hours = 8;

                if (_stie.MntScheduleTimes.ContainsKey(schedInst))
                {
                    if (schedInst == DateTime.Now.Date)
                    {
                        if (DateTime.Now <= _stie.MntScheduleTimes[schedInst].StartInst)
                        {
                            hours = _stie.MntScheduleTimes[schedInst].StartInst.Subtract(schedInst).TotalHours;
                        }
                        else if (_stie.MntScheduleTimes[schedInst].StartInst < DateTime.Now && DateTime.Now <= _stie.MntScheduleTimes[schedInst].EndInst)
                        {
                            hours = DateTime.Now.Subtract(schedInst).TotalHours + 0.5;
                        }
                        else if (_stie.MntScheduleTimes[schedInst].EndInst < DateTime.Now)
                        {
                            hours = DateTime.Now.Subtract(schedInst).TotalHours + 0.5;
                        }
                    }
                    else
                    {
                        hours = _stie.MntScheduleTimes[schedInst].StartInst.Subtract(schedInst).TotalHours;
                    }
                }

                foreach (MtrMntJob mntJob in mtrEngineer.AssignedMntJobs[schedInst].OrderByDescending(x => x.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished).ThenByDescending(x => x.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started))
                {
                    mntJob.ScheduledMntStartInst = schedInst.AddHours(hours);
                    hours += mntJob.MtrEvent.RequiredMntHours;
                }
            }
        }


        public MntCalculation(Site site, List<MtrMntJob> mntJobs, List<MtrEngineer> mtrEngineers)
        {
            _stie = site;
            _mntJobs = mntJobs;
            _mtrEngineers = mtrEngineers;

        }
    }
}