﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.Applications.Extensions;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrEngineer : BaseEntity
    {
        /// <summary>
        /// full name of engineer
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// the available schedule on this engineer on each site
        /// first key : site Id
        /// second key: date
        /// second value : how many hours available on that date
        /// </summary>
        public Dictionary<string, Dictionary<DateTime, double>> AvailableHours { get; set; }

        /// <summary>
        /// the skills and skill level
        /// skill level should be from 1 - 10
        /// 10 is good
        /// 1 is bad
        /// </summary>
        public Dictionary<string, MtrEngineerSkill> MtrEngineerSkills { get; set; }

        /// <summary>
        /// the current assigned jobs
        /// </summary>
        public Dictionary<DateTime, List<MtrMntJob>> AssignedMntJobs { get; set; }

        public List<MtrMntJob> TodayAssignedMntJobs
        {
            get
            {

                List<MtrMntJob> mtrMntJobs = new List<MtrMntJob>();
                foreach (Site site in Sites.Values)
                {
                    foreach (MtrMntJob mntMntJob in site.MtrMntJobs.Where(x => DateTime.Now.Date <= x.DisplayMntStart && x.DisplayMntStart < DateTime.Now.Date.AddDays(1)))
                    {
                        if (mntMntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished && mntMntJob.ActualMtrEngineer != null && mntMntJob.ActualMtrEngineer == this)
                        {
                            mtrMntJobs.Add(mntMntJob);
                        }
                        else if (mntMntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started && mntMntJob.ActualMtrEngineer != null && mntMntJob.ActualMtrEngineer == this)
                        {
                            mtrMntJobs.Add(mntMntJob);
                        }
                        else if (mntMntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled && mntMntJob.ScheduledMtrEngineer != null && mntMntJob.ScheduledMtrEngineer == this)
                        {
                            mtrMntJobs.Add(mntMntJob);
                        }
                    }
                }

                return mtrMntJobs;
            }
        }

        /// <summary>
        /// next Mnt job of today
        /// </summary>
        public MtrMntJob NextMtrMntJob
        {
            get
            {
                MtrMntJob mtrMntJob = null;
                DateTime today = DateTime.Now.Date;

                if (TodayAssignedMntJobs.HasData())
                {
                    mtrMntJob = TodayAssignedMntJobs.Where(x => DateTime.Now <= x.DisplayMntEnd).OrderBy(x => x.DisplayMntStart).FirstOrDefault();
                }
                return mtrMntJob;
            }
        }

        public string TodayScheduleDetails { get; set; }

        /// <summary>
        /// the list of site that this engineer is serving
        /// </summary>
        public Dictionary<string, Site> Sites { get; set; }

        /// <summary>
        /// the list of attentions
        /// </summary>
        public List<MtrAttention> CurrMtrAttentions { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        /// <param name="fullName"></param>
        public MtrEngineer(string id, string displayName, string fullName) : base(id, displayName)
        {
            FullName = fullName;
            AvailableHours = new Dictionary<string, Dictionary<DateTime, double>>();
            AssignedMntJobs = new Dictionary<DateTime, List<MtrMntJob>>();
            Sites = new Dictionary<string, Site>();
            MtrEngineerSkills = new Dictionary<string, MtrEngineerSkill>();

            CurrMtrAttentions = new List<MtrAttention>();
        }

    }
}