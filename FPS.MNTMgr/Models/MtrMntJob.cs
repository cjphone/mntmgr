﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrMntJob
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }

        public MtrEvent MtrEvent { get; set; }
        public MtrEquipment MtrEquipment { get; set; }

        public DateTime EarlyDueMntStartInst { get; set; }
        public DateTime LateDueMntStartInst { get; set; }

        public int MntPriority { get; set; }

        public List<MtrEngineer> EligibleMtrEngineers { get; set; }

        public MtrEngineer ScheduledMtrEngineer { get; set; }
        public DateTime? ScheduledMntStartInst { get; set; }

        public MtrEngineer ActualMtrEngineer { get; set; }
        public DateTime? ActualMntStartInst { get; set; }
        public DateTime? ActualMntEndInst { get; set; }

        /// <summary>
        /// only be
        /// 1. Due
        /// 2. Pending
        /// 3. Started
        /// 4. Scheduled
        /// 5. Finished
        /// </summary>
        public string ActualStatus
        {
            get
            {
                if (ScheduledMtrEngineer != null && ActualMntEndInst != null && ActualMntStartInst != null)
                {
                    return FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished;
                }
                else if (ScheduledMtrEngineer != null && ActualMntEndInst == null && ActualMntStartInst != null)
                {
                    return FPS.MNTMgr.Settings.AppSettings.MntJobState.Started;
                }
                else if (ScheduledMtrEngineer != null && ActualMntEndInst == null && ActualMntStartInst == null && ScheduledMntStartInst != null)
                {
                    return FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled;
                }
                else if (LateDueMntStartInst <= DateTime.Now)
                {
                    return FPS.MNTMgr.Settings.AppSettings.MntJobState.Due;
                }

                return FPS.MNTMgr.Settings.AppSettings.MntJobState.Pending;
            }
        }

        /// <summary>
        /// only be
        /// 1. Due
        /// 2. Pending
        /// 3. Started
        /// 4. Scheduled
        /// 5. Finished
        /// </summary>
        public int ActualStatusSort
        {
            get
            {
                if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Due)
                {
                    return 2;
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Pending)
                {
                    return 4;
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started)
                {
                    return 1;
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled)
                {
                    return 3;
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished)
                {
                    return 5;
                }
                return 100;
            }
        }

        public string ActualStatusColor
        {
            get
            {
                if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Due)
                {
                    return "#b22222";
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Pending)
                {
                    return "#cc9900";
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started)
                {
                    return "#009933";
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled)
                {
                    return "#ff9900";
                }
                else if (ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished)
                {
                    return "#3366ff";
                }
                return "#aaaaaa";
            }
        }




        public DateTime DisplayMntStart
        {
            get
            {
                if (ActualMntStartInst != null)
                    return (DateTime)ActualMntStartInst;
                else if (ScheduledMntStartInst != null)
                    return (DateTime)ScheduledMntStartInst;
                else
                    return LateDueMntStartInst;

            }
        }

        public DateTime DisplayMntEnd
        {
            get
            {
                if (ActualMntStartInst != null)
                    return ((DateTime)ActualMntStartInst).AddHours(MtrEvent.RequiredMntHours);
                else if (ScheduledMntStartInst != null)
                    return ((DateTime)ScheduledMntStartInst).AddHours(MtrEvent.RequiredMntHours);
                else
                    return LateDueMntStartInst.AddHours(MtrEvent.RequiredMntHours);

            }
        }

        /// <summary>
        /// job description
        /// </summary>
        public string Description { get; set; }

        public void CompileEligibleMtrEngineers(List<MtrEngineer> mtrEngineers)
        {
            EligibleMtrEngineers = new List<MtrEngineer>();

            foreach (MtrEngineer mtrEngineer in mtrEngineers)
            {
                if (!MtrEvent.RequiredMtrSkills.Values.Any(x => !mtrEngineer.MtrEngineerSkills.Any(y => y.Key == x.Id)))
                {
                    EligibleMtrEngineers.Add(mtrEngineer);
                }
            }
        }

        public void Reset(List<MtrEngineer> mtrEngineers)
        {
            MntPriority = 1;
            ScheduledMtrEngineer = null;
            ScheduledMntStartInst = null;
            ActualMntStartInst = null;
            ActualMntEndInst = null;
            CompileEligibleMtrEngineers(mtrEngineers);
        }

        public MtrMntJob(string id, MtrEvent mtrEvent, MtrEquipment mtrEquipment)
        {
            Id = id;
            MtrEvent = mtrEvent;
            MtrEquipment = mtrEquipment;
            MntPriority = 1;
            EligibleMtrEngineers = new List<MtrEngineer>();
        }
    }
}