﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.SolverFoundation.Common;
using Microsoft.SolverFoundation.Services;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrDecision
    {

        public string Id { get; set; }
        public MtrMntJob MntJob { get; set; }
        public MtrEngineer MtrEngineer { get; set; }
        public DateTime CheckInst { get; set; }

        public Decision Decision { get; set; }
        public double Score { get; set; }

        public MtrDecision(MtrMntJob mntJob, MtrEngineer mtrEngineer, DateTime checkInst)
        {
            MntJob = mntJob;
            MtrEngineer = mtrEngineer;
            CheckInst = checkInst;

            Id = "JobAsgn_" + mntJob.Id + "_" + mntJob.MtrEvent.Id + "_" + MntJob.MtrEquipment.Id + "_" + mtrEngineer.Id;

            if (mntJob.LateDueMntStartInst <= checkInst)
            {
                Score = 2 * mntJob.MtrEvent.RequiredMntHours;
            }
            else if (mntJob.EarlyDueMntStartInst <= checkInst)
            {
                Score = 1 * mntJob.MtrEvent.RequiredMntHours;
            }
            else
            {
                Score = -1 * mntJob.MtrEvent.RequiredMntHours;
            }
        }
    }
}