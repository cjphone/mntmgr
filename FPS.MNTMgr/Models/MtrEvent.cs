﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrEvent : BaseEntity
    {
        /// <summary>
        /// the site this event belongs to
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// the description of this event
        /// </summary>
        public List<string> Notes { get; set; }

        /// <summary>
        /// the list of check items, this is usually would be used to give users an idea what to do/check
        /// </summary>
        public List<string> CheckItems { get; set; }

        /// <summary>
        /// required skills to manage this event
        /// </summary>
        public Dictionary<string, MtrSkill> RequiredMtrSkills { get; set; }

        /// <summary>
        /// overall, the average mnt hours it required
        /// </summary>
        public double RequiredMntHours { get; set; }

        /// <summary>
        /// a new instance
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public MtrEvent(Site site, string id, string displayName, double requiredHours) : base(id, displayName)
        {
            Site = site;
            Notes = new List<string>();
            CheckItems = new List<string>();
            RequiredMtrSkills = new Dictionary<string, MtrSkill>();
            RequiredMntHours = requiredHours;
        }
    }
}