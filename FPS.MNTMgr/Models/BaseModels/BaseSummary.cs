﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.MNTMgr.Models.BaseModels
{
    public class BaseSummary : BaseEntity
    {

        /// <summary>
        /// the site this bag is in
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// the list of Mnt Entities, it can be the tool, or stocker
        /// </summary>
        public Dictionary<string, Tool> Tools { get; set; }

        public bool ShouldDisplay
        {
            get
            {
                if (Tools != null && Tools.Any(x => x.Value.Availability != "NST"))
                {
                    return true;
                }
                return false;
            }
        }

        public int NumToolsUp => Tools.Values.Count(x => x.Availability == "UP");
        public int NumToolsDown => Tools.Values.Count(x => x.Availability == "DOWN");
        public int NumToolsNST => Tools.Values.Count(x => x.Availability == "NST");

        public int NumChamberUp => Tools.Values.Sum(x => x.NumChamberUp);
        public int NumChamberDown => Tools.Values.Sum(x => x.NumChamberDown);
        public int NumChamberNST => Tools.Values.Sum(x => x.NumChamberNST);

        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="site"></param>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public BaseSummary(string id, string displayName) : base(id, displayName)
        {
            Tools = new Dictionary<string, Tool>();
        }

    }
}
