﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Models;

namespace FPS.MNTMgr.Models.BaseModels
{
    public class BaseEntity
    {
        /// <summary>
        /// entity id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// display name
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// the Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// sort order
        /// </summary>
        public int SortOrder { get; set; }

        public string State { get; set; }

        public string StateDisplay { get; set; }

        public DateTime StateChangeInst { get; set; }

        public virtual string HoverMessage { get; set; }

        public WebStyle WebStyle { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public BaseEntity(string id, string displayName)
        {
            Id = id;
            DisplayName = displayName;

        }
    }
}
