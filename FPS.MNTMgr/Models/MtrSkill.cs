﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrSkill : BaseEntity
    {
        /// <summary>
        /// the site this skill belongs to
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// initialize a new instance
        /// </summary>
        /// <param name="site"></param>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public MtrSkill(Site site, string id, string displayName) : base(id, displayName)
        {
            Site = site;
        }
    }
}