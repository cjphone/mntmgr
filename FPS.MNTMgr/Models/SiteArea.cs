﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class SiteArea : BaseEntity
    {
        /// <summary>
        /// the site this bag is in
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// the list of Mnt Entities, it can be the tool, or stocker
        /// </summary>
        public Dictionary<string, MtrEquipment> MtrEquipments { get; set; }

        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="site"></param>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public SiteArea(Site site, string id, string displayName) : base(id, displayName)
        {
            Site = site;
            MtrEquipments = new Dictionary<string, MtrEquipment>();
        }
    }
}
