﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrEngineerSkill
    {
        /// <summary>
        /// site
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// engineer
        /// </summary>
        public MtrEngineer MtrEngineer { get; set; }

        /// <summary>
        /// specific skill that this engineer has
        /// </summary>
        public MtrSkill MtrSkill { get; set; }

        /// <summary>
        /// the skill level, from 1 to 10; 10 is the best
        /// </summary>
        private double skillLevel;
        public double SkillLevel
        {
            get
            {

                if (skillLevel > 10)
                    return 10;
                else if (skillLevel <= 1)
                    return 1;
                else
                    return skillLevel;
            }
            set
            {
                skillLevel = value;
            }
        }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// initialize a new instance
        /// </summary>
        /// <param name="site"></param>
        /// <param name="mtrEngineer"></param>
        /// <param name="mtrSkill"></param>
        /// <param name="skillLevel"></param>
        public MtrEngineerSkill(Site site, MtrEngineer mtrEngineer, MtrSkill mtrSkill, double skillLevel)
        {
            Site = site;
            MtrEngineer = mtrEngineer;
            MtrSkill = mtrSkill;

            SkillLevel = skillLevel;
        }
    }
}
