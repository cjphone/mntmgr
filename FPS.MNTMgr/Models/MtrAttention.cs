﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrAttention
    {
        /// <summary>
        /// which entity, can be equipment or engineer
        /// </summary>
        public BaseEntity Entity { get; set; }

        /// <summary>
        /// current priority sort
        /// </summary>
        public int CurrPrioirySort { get; set; }

        /// <summary>
        /// current priority display
        /// </summary>
        public string CurrPrioiryDisplay { get; set; }

        /// <summary>
        /// when does it happen
        /// </summary>
        public DateTime Inst { get; set; }

        /// <summary>
        /// the engineer assigned for this attention
        /// </summary>
        public MtrEngineer AssignedEngineer { get; set; }

        /// <summary>
        /// when this assignment happened
        /// </summary>
        public DateTime? AssignedInst { get; set; }

        /// <summary>
        /// what is the status on this attention
        /// </summary>
        public string CurrState { get; set; }

        /// <summary>
        /// initialize a new instance
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="inst"></param>
        public MtrAttention(BaseEntity entity, DateTime inst)
        {
            Entity = entity;
            Inst = inst;

            CurrState = FPS.MNTMgr.Settings.AppSettings.MtrAttentionState.Unassigned;
        }
    }
}
