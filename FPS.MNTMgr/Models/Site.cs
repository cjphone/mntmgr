﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Extensions;
using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class Site : BaseEntity
    {
        /// <summary>
        /// the list of areas in this sites
        /// </summary>
        public Dictionary<string, SiteArea> SiteAreas { get; set; }

        /// <summary>
        /// the list of equipment under this site
        /// </summary>
        public Dictionary<string, MtrEquipment> MtrEquipments
        {
            get
            {
                Dictionary<string, MtrEquipment> mtrEquipments = new Dictionary<string, MtrEquipment>();

                foreach (SiteArea area in SiteAreas.Values.OrderBy(x => x.Id))
                {
                    foreach (MtrEquipment eqp in area.MtrEquipments.Values.OrderBy(x => x.Id))
                    {
                        mtrEquipments.TryAdd(eqp.Id, eqp);
                    }
                }

                return mtrEquipments;
            }
        }

        public List<MtrMntJob> MtrMntJobs
        {
            get
            {
                List<MtrMntJob> mtrMntJobs = new List<MtrMntJob>();

                foreach (SiteArea area in SiteAreas.Values.OrderBy(x => x.Id))
                {
                    foreach (MtrEquipment eqp in area.MtrEquipments.Values.OrderBy(x => x.Id))
                    {
                        foreach (MtrMntJob mtrMntJob in eqp.RequiredMntJobs.OrderBy(x => x.Id))
                        {
                            mtrMntJobs.TryAdd(mtrMntJob);
                        }
                    }
                }

                return mtrMntJobs;
            }
        }

        public List<MtrAttention> CurrMtrAttentions
        {
            get
            {
                List<MtrAttention> currMtrAttentions = new List<MtrAttention>();

                foreach (SiteArea area in SiteAreas.Values)
                {
                    foreach (MtrEquipment eqp in area.MtrEquipments.Values)
                    {

                        currMtrAttentions.AddRange(eqp.CurrMtrAttentions);

                    }
                }

                foreach(MtrEngineer engineer in MtrEngineers.Values)
                {
                    currMtrAttentions.AddRange(engineer.CurrMtrAttentions);
                }

                return currMtrAttentions;
            }
        }

        /// <summary>
        /// the list of eligible skills
        /// </summary>
        public Dictionary<string, MtrSkill> MtrSkills { get; set; }

        /// <summary>
        /// the list of events associated with this site
        /// </summary>
        public Dictionary<string, MtrEvent> MtrEvents { get; set; }

        /// <summary>
        /// the list of engineers works under this site
        /// </summary>
        public Dictionary<string, MtrEngineer> MtrEngineers { get; set; }

        /// <summary>
        /// schedule mnt working time for everydate
        /// </summary>
        public Dictionary<DateTime, ScheduleTime> MntScheduleTimes { get; set; }

        /// <summary>
        /// contractor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public Site(string id, string displayName) : base(id, displayName)
        {
            SiteAreas = new Dictionary<string, SiteArea>();
            MtrSkills = new Dictionary<string, MtrSkill>();
            MtrEvents = new Dictionary<string, MtrEvent>();
            MtrEngineers = new Dictionary<string, Models.MtrEngineer>();


            //initialize
            //this originally should came from Db
            MntScheduleTimes = new Dictionary<DateTime, ScheduleTime>();
            for (int i = -60; i <= 120; i++)
            {
                MntScheduleTimes.Add(DateTime.Now.Date.AddDays(i), new ScheduleTime(DateTime.Now.Date.AddDays(i).AddHours(8), DateTime.Now.Date.AddDays(i).AddHours(20)));
            }
        }
    }
}
