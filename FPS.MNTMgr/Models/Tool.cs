﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class Tool : BaseEntity
    {
        public Bay Bay { get; set; }
        public Dictionary<string, ToolChamber> Chambers { get; set; }
        public Dictionary<string, ToolPort> Ports { get; set; }
        public string ToolType { get; set; }
        public string Availability { get; set; }
        public int ReserveCapacity { get; set; }
        public int ReserveSec { get; set; }
        public int NumChamberUp => Chambers.Values.Count(x => x.Availability == "UP");
        public int NumChamberDown => Chambers.Values.Count(x => x.Availability == "DOWN");
        public int NumChamberNST => Chambers.Values.Count(x => x.Availability == "NST");

        public override string HoverMessage
        {
            get
            {
                return "<div> Tool Id:" + DisplayName + "</div>" +
                    "<div> Availability:" + Availability + "</div>" +
                    "<div> State:" + StateDisplay + "</div>" +
                    "<div> State Changed Time:" + StateChangeInst.ToString() + "</div>";
            }


        }

        public Tool(Bay bay, string id, string displayName) : base(id, displayName)
        {
            Chambers = new Dictionary<string, ToolChamber>();
            Ports = new Dictionary<string, ToolPort>();
            Availability = "NST";
        }

    }
}
