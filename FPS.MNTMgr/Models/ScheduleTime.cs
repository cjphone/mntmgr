﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.MNTMgr.Models
{
    public class ScheduleTime
    {
        /// <summary>
        /// start inst on this schedule time
        /// </summary>
        public virtual DateTime StartInst { get; set; }

        private DateTime endInst;

        /// <summary>
        /// end inst on this schedule time
        /// </summary>
        public virtual DateTime EndInst
        {
            get
            {
                if (endInst < StartInst)
                {
                    return StartInst.AddSeconds(1);
                }
                else
                {
                    return endInst;
                }
            }
            set { endInst = value; }

        }

        /// <summary>
        /// duration on this schedule time
        /// </summary>
        public TimeSpan Duration { get { return EndInst.Subtract(StartInst); } }

        /// <summary>
        /// initialize a new instance
        /// </summary>
        /// <param name="startInst"></param>
        /// <param name="endInst"></param>
        public ScheduleTime(DateTime startInst, DateTime endInst)
        {
            StartInst = startInst;
            EndInst = endInst;
        }
    }
}
