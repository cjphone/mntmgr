﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class ProcessFamily : BaseSummary
    {
        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="site"></param>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public ProcessFamily(string id, string displayName) : base(id, displayName)
        {
            Tools = new Dictionary<string, Tool>();
        }
    }
}
