﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class ToolPort : BaseEntity
    {
        public Tool Tool { get; set; }

        public override string HoverMessage
        {
            get
            {
                return "<div> Port:" + DisplayName + "</div>" +
                    "<div> Port Mode:" + StateDisplay + "</div>";
            }


        }

        public ToolPort(Tool tool, string id, string displayName) : base(id, displayName)
        {
            Tool = tool;
        }
    }
}
