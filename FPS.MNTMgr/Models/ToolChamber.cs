﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class ToolChamber : BaseEntity
    {
        public Tool Tool { get; set; }
        public string Availability { get; set; }

        public override string HoverMessage
        {
            get
            {
                return "<div> Chamber:" + DisplayName + "</div>" +
                    "<div> State:" + StateDisplay + "</div>";
            }


        }

        public ToolChamber(Tool tool, string id, string displayName) : base(id, displayName)
        {
            Tool = tool;
            Availability = "NST";
        }
    }
}
