﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models.BaseModels;

namespace FPS.MNTMgr.Models
{
    public class MtrEquipment : BaseEntity
    {
        /// <summary>
        /// the area
        /// </summary>
        public SiteArea Area { get; set; }

        /// <summary>
        /// the maintenance family
        /// </summary>
        public string MainEquipmentId { get; set; }

        /// <summary>
        /// type : tool, chamber, reticle, sorter, amhs or whatever equipment
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// current state display
        /// </summary>
        public string CurrStateDisplay { get; set; }

        /// <summary>
        /// when did this state happen
        /// </summary>
        public DateTime CurrStateStartInst { get; set; }

        /// <summary>
        /// current state display HTML Style
        /// </summary>
        public string CurrStateDisplayClassName { get; set; }

        /// <summary>
        /// the children eqpipments 
        /// </summary>
        public List<MtrEquipment> SubEquipments { get; set; }

        /// <summary>
        /// required mnt jobs, including historical data and future data
        /// </summary>
        public List<MtrMntJob> RequiredMntJobs { get; set; }

        /// <summary>
        /// the list of attentions
        /// </summary>
        public List<MtrAttention> CurrMtrAttentions { get; set; }

        /// <summary>
        /// create a new instance
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayName"></param>
        public MtrEquipment(SiteArea area, string id, string displayName) : base(id, displayName)
        {
            Area = area;
            SubEquipments = new List<MtrEquipment>();
            RequiredMntJobs = new List<MtrMntJob>();

            CurrStateDisplay = "SBY";
            CurrStateStartInst = DateTime.Now.AddHours(-1);
            CurrStateDisplayClassName = "eqp_state_sby";

            CurrMtrAttentions = new List<MtrAttention>();
        }
    }
}