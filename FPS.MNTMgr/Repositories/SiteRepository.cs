﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models;
using FPS.MNTMgr.Services.DataAccess;

namespace FPS.MNTMgr.Repositories
{
    public static class SiteRepository
    {
        public static Dictionary<string, Site> Sites;
        public static Dictionary<string, MtrEngineer> MtrEngineers;

        public static void Init()
        {
            Sites = new Dictionary<string, Site>();
            MtrEngineers = new Dictionary<string, MtrEngineer>();

            try
            {
                ConnectionStringSettings defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["MNTMGRDWH"];

                CommDataAccess commDataAccess = new CommDataAccess(defaultConnectionStringSettings);

                commDataAccess.LoadData();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("initSites->" + ex.Message);
            }
        }


    }
}
