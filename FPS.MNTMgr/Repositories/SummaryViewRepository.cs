﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Models;
using FPS.MNTMgr.Services.DataAccess;

namespace FPS.MNTMgr.Repositories
{
    public static class SummaryViewRepository
    {
        public static Dictionary<string, Bay> Bays;
        public static Dictionary<string, ProcessFamily> ProcessFamilies;

        public static void Init()
        {
            try
            {
                ConnectionStringSettings defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["FPSDWH"];

                CommDataAccess commDataAccess = new CommDataAccess(defaultConnectionStringSettings);

                commDataAccess.LoadSummaryViewData();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("initBayView->" + ex.Message);
            }
        }


    }
}
