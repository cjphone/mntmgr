﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Common;

namespace FPS.MNTMgr.Settings
{
    /// <summary>
    /// application Setting
    /// </summary>
    public static class AppSettings
    {
        public const int MaxChecks = 5;
        public const string DoNotRefresh = "DO NOT REFRESH";
        public const string None = "None";
        public const string RealTimeDataId = "Real-Time Data";
        public const string NewDurableKey = "New Durable Key";
        public const string RunByTrigger = "TRIGGER";
        public const int MaxThreadingSec = 2;


        public static string ClientName = "FPS";
        public static string DataSource = "PROD";
        public static string DefaultSiteId;
        public static string Application;
        public static int AutoRefreshInSec;
        public static int AutoLogoutSec;
        public static string DashboardUrl;
        public static string FullCalendarDateTimeFormat = "yyyy/M/d H:mm:ss";

        public static DateTime LicenseExpirationDate = new DateTime(2020, 1, 1);
        public static string MainLogoUrl;
        public static string FeedbackRecipients;
        public static string FeedbackScriptUrl;
        public static int NumTopToolAssignments = 3;

        public static bool EnableDebugMessages = false;
        public static bool EnableParallelComputing = true;
        public static double DebugCalTimeoutSec = 1;
        public static bool EnableAutoRefresh = false;

        public static TimeSpan DefaultTransitTime = TimeSpan.FromHours(2);

        public static int MaxYearsToSchedule = 10;
        public static DateTime EndOfSchedulerTime = new DateTime(2020, 1, 1);
        public static double MinScore = double.MinValue + 1000000;
        public static Dictionary<string, string> HtmlRegexTokens;
        public static double AvgNumDurablesBuiltPerPod = 3.5;
        public static int MaxWhileLoopCount = 12;
        public const string UnknownLocation = "UNKNOWN";

        public static double TimerHandCarryWindowInSec = 7200;

        public static double MinDefaultScheduleTimeHours = -1;
        public static double MaxDefaultScheduleTimeHours = 8760;
        public static double MaxBatchingWaitingInMins = 10; // this is used to indicate how late the coming lots can join this batch
        public static double MaxCascadingWaitingInMins = 3; // this is used to indicate how late the coming lots can join this batch


        public static double MaxBatchDispIntervalSec = 180;
        public static double MinBatchSizeWaitSecToSched = 3600;

        public static string Verion;

        public static List<string> ChTypesRequireDurables = new List<string>();

        public static Dictionary<string, double> ToolAsgnRanks;

        public static class CultureInfo
        {
            public static string DbHour = "HH";
            public static string DbDateTime = "";
            public static string DefaultCulture = "en-us";
        }


        public static class Format
        {
            public static string DateDisplay = "MM/dd/yy";
            public static string TimeDisplay = "HH:mm:ss";
            public static string DateTimeDisplay = "yyyy/MM/dd HH:mm:ss";
            public static int NumberDecimal = 1;
            public static string NumberDisplay = "N1";
        }

        public static class Message
        {
            public const string Error = "ERROR";
            public const string Success = "SUCCESS";
        }

        public static class RunState
        {
            public const string Success = "SUCCESS";
            public const string Slow = "SLOW";
            public const string Fail = "FAIL";
        }

        public static class Roles
        {
            public const string Disabled = "Disabled";
            public const string ReviewScoreDetails = "Review Score Details";
            public const string SchedGroupScenarioRun = "Sched Group Scenario Run";
            public const string SchedConfiguration = "Sched Group Configuration";
            public const string SchedGroupSettings = "Sched Group Settings";
            public const string SchedGroupInDev = "Sched Group In Dev";
            public const string SystemSettings = "System Settings";
        }

        public static class MntJobState
        {
            public const string Finished = "Finished";
            public const string Started = "Started";
            public const string Scheduled = "Scheduled";
            public const string Due = "Due";
            public const string Pending = "Pending";

        }

        public static class MtrAttentionState
        {
            public const string Unassigned = "Unassigned";
            public const string Assigned = "Assigned";
            public const string Verifying = "Verifying";
            public const string Verified = "Verified";
            public const string Other = "Other";

        }


        public static class EqpAvailability
        {
            public const string UP = "UP";
            public const string DOWN = "DOWN";
            public const string NST = "NST";

        }

        public static class EqpMainEntity
        {
            public const string Id = "M";
            public const string Display = "MAIN";

        }

        public static class SessionName
        {
            public const string SchedGroupScenarioRun = "SCENARIO_RUN";
            public const string ToolGantChartSort = "SORT_BY";
        }

        
        //public static class Durable
        //{
        //    public const double MinValue = 0.1;
        //    public const double Base = 1;
        //}

        /// <summary>
        /// initial
        /// </summary>
        public static void Init()
        {
            EndOfSchedulerTime = new DateTime(DateTime.Now.Year + MaxYearsToSchedule + 1, 1, 1);

            CultureInfo.DbDateTime = ConfigurationManager.AppSettings["CultureInfoDbDateTime"];
            CultureInfo.DbHour = ConfigurationManager.AppSettings["CultureInfoDbHour"];
            CultureInfo.DefaultCulture = string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultCulture"]) ? "en-us" : ConfigurationManager.AppSettings["DefaultCulture"];

            Format.DateTimeDisplay = ConfigurationManager.AppSettings["FormatDateTimeDisplay"];
            Format.NumberDecimal = string.IsNullOrEmpty(ConfigurationManager.AppSettings["FormatNumberDecimal"]) ? 1 : int.Parse(ConfigurationManager.AppSettings["FormatNumberDecimal"]);
            Format.NumberDisplay = ConfigurationManager.AppSettings["FormatNumberDisplay"];


            //ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["FPSDWH"];
            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionStringSettings.ConnectionString);
            //DataSource = builder.DataSource;



            ClientName = ConfigurationManager.AppSettings["ClientName"];
            DataSource = ConfigurationManager.AppSettings["DataSource"];
            DefaultSiteId = ConfigurationManager.AppSettings["DefaultSite"];
            Application = ConfigurationManager.AppSettings["Application"];

            //the num of top score/tool assignments to display
            NumTopToolAssignments = int.Parse(ConfigurationManager.AppSettings["NumTopToolAssignments"] ?? "3");

            MaxBatchDispIntervalSec = double.Parse(ConfigurationManager.AppSettings["MaxBatchDispIntervalSec"] ?? "180");
            TimerHandCarryWindowInSec = double.Parse(ConfigurationManager.AppSettings["TimerHandCarryWindowInSec"] ?? "7200");

            EnableDebugMessages = ConfigurationManager.AppSettings["EnableDebugMessages"].ToString().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            //string enableDebugMessages = (ConfigurationManager.AppSettings["EnableDebugMessages"]) ?? "N";
            //EnableDebugMessages = enableDebugMessages.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            EnableParallelComputing = ConfigurationManager.AppSettings["EnableParallelComputing"].ToString().Equals("Y", StringComparison.InvariantCultureIgnoreCase);
            EnableAutoRefresh = ConfigurationManager.AppSettings["EnableAutoRefresh"].ToString().Equals("Y", StringComparison.InvariantCultureIgnoreCase);

            DebugCalTimeoutSec = double.Parse(ConfigurationManager.AppSettings["DebugCalTimeoutSec"] ?? "60");


            //Auto Refresh & Stop Refresh
            AutoRefreshInSec = int.Parse(ConfigurationManager.AppSettings["AutoRefreshInSec"] ?? "60");
            AutoLogoutSec = int.Parse(ConfigurationManager.AppSettings["AutoLogoutSec"] ?? "600");

            DashboardUrl = ConfigurationManager.AppSettings["DashboardUrl"];
            MainLogoUrl = ConfigurationManager.AppSettings["MainLogoUrl"];

            FeedbackRecipients = ConfigurationManager.AppSettings["FeedbackRecipients"];
            FeedbackScriptUrl = ConfigurationManager.AppSettings["FeedbackScriptUrl"];

            Verion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            ChTypesRequireDurables = new List<string>();
            string chTypesRequireDurables = ConfigurationManager.AppSettings["ChTypesRequireDurables"];

            //get the list of chamber types that require the chamber to use durable
            if (!string.IsNullOrEmpty(chTypesRequireDurables))
            {
                ChTypesRequireDurables = chTypesRequireDurables.Split(',').ToList();
            }

            HtmlRegexTokens = new Dictionary<string, string>();

            HtmlRegexTokens.Add("<table", "*b");
            HtmlRegexTokens.Add("</table>", "#b");
            HtmlRegexTokens.Add("<tr", "*r");
            HtmlRegexTokens.Add("</tr>", "#r");
            HtmlRegexTokens.Add("<td", "*d");
            HtmlRegexTokens.Add("</td>", "#d");
            HtmlRegexTokens.Add("<th", "*h");
            HtmlRegexTokens.Add("</th>", "#h");
            HtmlRegexTokens.Add("class", "$c");
            HtmlRegexTokens.Add("style", "$s");
            HtmlRegexTokens.Add("colspan", "$n");
            HtmlRegexTokens.Add("title", "$t");


            ToolAsgnRanks = new Dictionary<string, double>();
            ToolAsgnRanks.Add("P", 10);
            ToolAsgnRanks.Add("A", 1);
            ToolAsgnRanks.Add("E", 0.01);
            ToolAsgnRanks.Add("I", -10);
        }


    }

}
