﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.MNTMgr.Languages.Models;
using FPS.MNTMgr.Languages.Services.DataAccess;

namespace FPS.MNTMgr.Languages.Repositories
{
    public static class LanguageRepository
    {
        public static List<SiteResource> SiteResources;
        public static List<SiteCulture> SiteCultures;

        public static void Init()
        {
            SiteResources = new List<SiteResource>();
            SiteCultures = new List<SiteCulture>();

            initResources();
        }

        private static void initResources()
        {
            try
            {
                ConnectionStringSettings defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["MNTMGRDWH"];

                CommDataAccess commDataAccess = new CommDataAccess(defaultConnectionStringSettings);

                commDataAccess.LoadSiteResources();
                commDataAccess.LoadSiteCultures();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("initSites->" + ex.Message);
            }
        }
    }
}
