﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Npgsql;


namespace FPS.MNTMgr.Languages.Services.DataAccess
{
    public class CommDataAccessDbWrite
    {
        private static readonly log4net.ILog fileLogger = log4net.LogManager.GetLogger("FileLogger");

        private ConnectionStringSettings defaultConnectionStringSettings;

        /// <summary>
        /// initialize a new instance for scheduler db data service by using connection string settings
        /// </summary>
        /// <param name="connectionStringSettings"></param>
        internal CommDataAccessDbWrite(ConnectionStringSettings connectionStringSettings)
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }

        internal string AddLanguageSiteResource(string siteId, string culture, string name, string value)
        {
            if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(culture) || string.IsNullOrEmpty(name) || string.IsNullOrEmpty(value))
            {
                return "Insufficient data. Please verify.";
            }
            else
            {
                string sqlStoredProcedure = "public.mtr_site_resources_insert";

                try
                {
                    //open the connection 
                    using (NpgsqlConnection conn = new NpgsqlConnection())
                    {


                        conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                        conn.Open();

                        #region using PgSql command
                        using (NpgsqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = sqlStoredProcedure;
                            cmd.CommandType = CommandType.StoredProcedure;

                            //start to plug variables in
                            cmd.Parameters.AddWithValue("in_site", siteId);
                            cmd.Parameters.AddWithValue("in_culture", culture);
                            cmd.Parameters.AddWithValue("in_name", name);
                            cmd.Parameters.AddWithValue("in_value", value);
                            cmd.Parameters.AddWithValue("in_description", value);

                            //run the execution
                            cmd.ExecuteNonQuery();


                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error("Error", ex);

                    return "Error, " + ex.Message;
                }

            }

            return "SUCCESS";
        }

    }
}
