﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Npgsql;

using FPS.MNTMgr.Languages.Models;

namespace FPS.MNTMgr.Languages.Services.DataAccess
{
    public class CommDataAccessDbRead
    {
        private static readonly log4net.ILog fileLogger = log4net.LogManager.GetLogger("FileLogger");

        private ConnectionStringSettings defaultConnectionStringSettings;

        /// <summary>
        /// initialize a new instance for scheduler db data service by using connection string settings
        /// </summary>
        /// <param name="connectionStringSettings"></param>
        internal CommDataAccessDbRead(ConnectionStringSettings connectionStringSettings)
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }

        internal List<SiteResource> GetSiteResources()
        {
            List<SiteResource> items = new List<SiteResource>();
            SiteResource item;

            try
            {
                string sqlSelect = string.Format(@"
                    select
                        site, 
                        name, 
                        culture,
                        value
                    from mtr_p_site_resources
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string name = reader["name"].ToString().Trim();
                                string culture = reader["culture"].ToString().Trim();
                                string value = reader["value"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(culture) && !string.IsNullOrEmpty(value))
                                {
                                    item = new SiteResource(siteId, name, culture, value);

                                    items.Add(item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error("Error", ex);
            }

            return items;
        }

        internal List<SiteCulture> GetSiteCultures()
        {
            List<SiteCulture> items = new List<SiteCulture>();
            SiteCulture item;

            try
            {
                string sqlSelect = string.Format(@"
                    select
                        site,
                        culture,
                        culture_calendar_file,
                        culture_name,
                        culture_display,
                        description,
                        is_supported,
                        is_default
                    from mtr_p_site_cultures
                    
                ");

                //open the connection and get the data
                using (NpgsqlConnection conn = new NpgsqlConnection())
                {
                    conn.ConnectionString = defaultConnectionStringSettings.ConnectionString;
                    conn.Open();

                    #region using oracle command
                    using (NpgsqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = sqlSelect;
                        //cmd.BindByName = true;

                        using (IDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                string siteId = reader["site"].ToString().Trim();
                                string culture = reader["culture"].ToString().Trim();
                                string cultureCalendarFile = reader["culture_calendar_file"].ToString().Trim();
                                string cultureName = reader["culture_name"].ToString().Trim();
                                string cultureDisplay = reader["culture_display"].ToString().Trim();
                                string description = reader["description"].ToString().Trim();
                                string isSupported = reader["is_supported"].ToString().Trim();
                                string isDefault = reader["is_default"].ToString().Trim();

                                if (!string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(culture) && !string.IsNullOrEmpty(cultureName))
                                {
                                    item = new SiteCulture(siteId, culture, cultureName)
                                    {
                                        CultureCalendarFile = cultureCalendarFile,
                                        CultureDisplay = cultureDisplay,
                                        Description = description,
                                        IsSupported = isSupported == "Y",
                                        IsDefault = isDefault == "Y"
                                    };

                                    items.Add(item);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error("Error", ex);
            }

            return items;
        }
    }
}
