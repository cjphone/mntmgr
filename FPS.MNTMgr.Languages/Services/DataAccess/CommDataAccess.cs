﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Extensions;
using FPS.MNTMgr.Languages.Models;
using FPS.MNTMgr.Languages.Repositories;

namespace FPS.MNTMgr.Languages.Services.DataAccess
{
    public class CommDataAccess
    {
        private FPS.Applications.Models.MessageLogs messageLogs;

        //data source
        private ConnectionStringSettings defaultConnectionStringSettings;

        private readonly CommDataAccessDbRead dbRead;
        private readonly CommDataAccessDbWrite dbWrite;

        public CommDataAccess(ConnectionStringSettings connectionStringSettings)
            : this(new CommDataAccessDbRead(connectionStringSettings), new CommDataAccessDbWrite(connectionStringSettings))
        {
            defaultConnectionStringSettings = connectionStringSettings;
        }

        private CommDataAccess(CommDataAccessDbRead csDbRead, CommDataAccessDbWrite csDbWrite)
        {
            dbRead = csDbRead;
            dbWrite = csDbWrite;
            messageLogs = new FPS.Applications.Models.MessageLogs();
        }

        public void LogMessage(string logAction, string logMsg)
        {
            //dbWrite.LogMessage(logAction, logMsg);
        }

        /// <summary>
        /// pre loading all data related to the site into the memory
        /// </summary>
        public void LoadSiteResources()
        {
            string message = "SUCCESS";

            try
            {
                List<SiteResource> siteResources = getSiteResources();

                //need to make sure it has all sites data and base data for each site
                if (siteResources.HasData())
                {
                    LanguageRepository.SiteResources = siteResources;
                }
                else
                {
                    message = "Error" + " : Sites Data having problems";
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("LoadResources->" + ex.Message);

                message = "Error" + " : " + ex.Message;
            }

            LogMessage("LoadSites", message);

        }

        public void LoadSiteCultures()
        {
            string message = "SUCCESS";

            try
            {
                List<SiteCulture> siteCultures = getSiteCultures();

                //need to make sure it has all sites data and base data for each site
                if (siteCultures.HasData())
                {
                    LanguageRepository.SiteCultures = siteCultures;
                }
                else
                {
                    message = "Error" + " : Sites Data having problems";
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("LoadResources->" + ex.Message);

                message = "Error" + " : " + ex.Message;
            }

            LogMessage("LoadSites", message);

        }

        /// <summary>
        /// get the sites info
        /// </summary>
        /// <returns></returns>
        private List<SiteResource> getSiteResources()
        {
            List<SiteResource> siteResources = dbRead.GetSiteResources();

            //loadSiteDatas(sites);

            return siteResources;

        }

        private List<SiteCulture> getSiteCultures()
        {
            List<SiteCulture> siteCultures = dbRead.GetSiteCultures();

            //loadSiteDatas(sites);

            return siteCultures;

        }

        public string AddLanguageSiteResource(string siteId, string culture, string name, string value)
        {

            SiteResource siteResrouce = LanguageRepository.SiteResources.FirstOrDefault(x => x.SiteId == siteId && x.Culture == culture && x.Name == name);

            if (siteResrouce != null)
            {
                siteResrouce.Value = value;
            }
            else
            {
                siteResrouce = new SiteResource(siteId, name, culture, value);
                LanguageRepository.SiteResources.Add(siteResrouce);
            }

            return dbWrite.AddLanguageSiteResource(siteId, culture, name, value);
        }


    }
}
