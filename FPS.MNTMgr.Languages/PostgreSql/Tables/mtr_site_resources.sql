﻿-- Table: public.mtr_site_resources

-- DROP TABLE public.mtr_site_resources;

CREATE TABLE public.mtr_site_resources
(
  site character varying(16) NOT NULL,
  culture character varying(16) NOT NULL,
  name character varying(256) NOT NULL,
  value character varying(256) NOT NULL,
  description character varying(256),
  CONSTRAINT mtr_site_resources_pk PRIMARY KEY (site, culture, name),
  CONSTRAINT mtr_site_resources_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_site_resources
  OWNER TO mntmgr;
