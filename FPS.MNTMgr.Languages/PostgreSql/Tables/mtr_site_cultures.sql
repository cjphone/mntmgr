﻿-- Table: public.mtr_site_cultures

-- DROP TABLE public.mtr_site_cultures;

CREATE TABLE public.mtr_site_cultures
(
  site character varying(16) NOT NULL,
  culture character varying(16) NOT NULL,
  culture_calendar_file character varying(32),
  culture_name character varying(128) NOT NULL,
  culture_display character varying(128) NOT NULL,
  description character varying(512),
  is_default character varying(1) NOT NULL,
  is_supported character varying(1) NOT NULL DEFAULT 'N'::bpchar,
  CONSTRAINT mtr_site_cultures_pk PRIMARY KEY (site, culture),
  CONSTRAINT mtr_site_cultures_fk_site FOREIGN KEY (site)
      REFERENCES public.mtr_sites (site) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.mtr_site_cultures
  OWNER TO mntmgr;
