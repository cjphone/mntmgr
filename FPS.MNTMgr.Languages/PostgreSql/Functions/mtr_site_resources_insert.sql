﻿-- Function: public.mtr_site_resources_insert(character, character, character, character varying, character varying)

-- DROP FUNCTION public.mtr_site_resources_insert(character, character, character, character varying, character varying);

CREATE OR REPLACE FUNCTION public.mtr_site_resources_insert(
    in_site character,
    in_culture character,
    in_name character,
    in_value character varying,
    in_description character varying)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_site_resources (site, culture, name, value, description) 
	values (in_site, in_culture, in_name, in_value, in_description)
	on conflict (site, culture, name)
	do update set (value, description) = (in_value, in_description);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_site_resources_insert(character, character, character, character varying, character varying)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_site_resources_insert(character, character, character, character varying, character varying) IS 'insert new event into mtr_site_resources';
