﻿-- Function: public.mtr_site_cultures_insert(character, character, character, character, character varying, character varying, character, character)

-- DROP FUNCTION public.mtr_site_cultures_insert(character, character, character, character, character varying, character varying, character, character);

CREATE OR REPLACE FUNCTION public.mtr_site_cultures_insert(
    in_site character,
    in_culture character,
    in_culture_calendar_file character,
    in_culture_name character,
    in_culture_display character varying,
    in_description character varying,
    in_is_default character,
    in_is_supported character)
  RETURNS character AS
$BODY$
BEGIN 


	insert into public.mtr_site_cultures (site, culture, culture_calendar_file, culture_name, culture_display, description, is_default, is_supported) 
	values (in_site, in_culture, in_culture_calendar_file, in_culture_name, in_culture_display, in_description, in_is_default, in_is_supported)
	on conflict (site, culture)
	do update set (culture_calendar_file, culture_name, culture_display, description, is_default, is_supported) = (in_culture_calendar_file, in_culture_name, in_culture_display, in_description, in_is_default, in_is_supported);

	RETURN 'Inserted';
	
EXCEPTION when others then 
	--raise notice '% %', SQLERRM, SQLSTATE;
	RETURN 'Failed:' || SQLERRM || ',' || SQLSTATE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.mtr_site_cultures_insert(character, character, character, character, character varying, character varying, character, character)
  OWNER TO mntmgr;
COMMENT ON FUNCTION public.mtr_site_cultures_insert(character, character, character, character, character varying, character varying, character, character) IS 'insert new event into mtr_site_cultures';
