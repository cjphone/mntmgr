﻿-- View: public.mtr_p_site_cultures

-- DROP VIEW public.mtr_p_site_cultures;

CREATE OR REPLACE VIEW public.mtr_p_site_cultures AS 
 SELECT mtr_site_cultures.site,
    mtr_site_cultures.culture,
    mtr_site_cultures.culture_calendar_file,
    mtr_site_cultures.culture_name,
    mtr_site_cultures.culture_display,
    mtr_site_cultures.description,
    mtr_site_cultures.is_default,
    mtr_site_cultures.is_supported
   FROM mtr_site_cultures
  WHERE mtr_site_cultures.is_supported::bpchar = 'Y'::bpchar;

ALTER TABLE public.mtr_p_site_cultures
  OWNER TO mntmgr;
