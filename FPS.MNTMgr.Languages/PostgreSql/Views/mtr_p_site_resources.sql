﻿-- View: public.mtr_p_site_resources

-- DROP VIEW public.mtr_p_site_resources;

CREATE OR REPLACE VIEW public.mtr_p_site_resources AS 
 SELECT mtr_site_resources.site,
    mtr_site_resources.culture,
    mtr_site_resources.name,
    mtr_site_resources.value,
    mtr_site_resources.description
   FROM mtr_site_resources;

ALTER TABLE public.mtr_p_site_resources
  OWNER TO mntmgr;
