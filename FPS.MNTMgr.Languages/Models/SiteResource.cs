﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.MNTMgr.Languages.Models
{
    public class SiteResource
    {
        /// <summary>
        /// site Id
        /// </summary>
        public string SiteId { get; set; }

        /// <summary>
        /// the name of language entity, for example "Logic", "Please provide valid username"
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// en-US, 
        /// ja-JP, 
        /// zh-CN, 
        /// zh-TW
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// the value, can be English, Japanese, English
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// new a instance
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="name"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        public SiteResource(string siteId, string name, string culture, string value)
        {
            SiteId = siteId;
            Name = name;
            Culture = culture;
            Value = value;

        }
    }
}
