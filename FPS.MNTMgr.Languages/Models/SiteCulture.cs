﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.MNTMgr.Languages.Models
{
    public class SiteCulture
    {
        /// <summary>
        /// site Id
        /// </summary>
        public string SiteId { get; set; }

        /// <summary>
        /// en-US, 
        /// ja-JP, 
        /// zh-CN, 
        /// zh-TW
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// the calendar file that should use
        /// </summary>
        public string CultureCalendarFile { get; set; }

        /// <summary>
        /// the culture name, readable in english
        /// </summary>
        public string CultureName { get; set; }

        /// <summary>
        /// the culture display, readable in its language
        /// </summary>
        public string CultureDisplay { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// indicate if this language supported by this site
        /// </summary>
        public bool IsSupported { get; set; }

        /// <summary>
        /// indicate the default culture this site is using
        /// </summary>
        public bool IsDefault { get; set; }


        /// <summary>
        /// new a instance
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="name"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        public SiteCulture(string siteId, string culture, string cultureName)
        {
            SiteId = siteId;
            Culture = culture;
            CultureName = cultureName;

        }
    }
}
