﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FPS.Applications.Extensions;
using FPS.MNTMgr.Languages.Repositories;

namespace FPS.MNTMgr.Languages.Models
{
    public class Resources
    {
        /// <summary>Add person</summary>
        public static string GetText(string siteId, string name, string culture)
        {

            if (LanguageRepository.SiteResources.HasData() && !string.IsNullOrEmpty(siteId) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(culture))
            {
                SiteResource resource = LanguageRepository.SiteResources.FirstOrDefault(x => x.SiteId == siteId && x.Name == name && x.Culture.Equals(culture, StringComparison.InvariantCultureIgnoreCase));

                if (resource != null)
                {
                    return resource.Value;
                }
            }

            return name;

        }
    }
}
