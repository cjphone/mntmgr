﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Repositories;
using FPS.MNTMgr.Languages.Repositories;

namespace FPS.MNTMgr.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            AppSettings.Init();
            SiteRepository.Init();
            SummaryViewRepository.Init();
            LanguageRepository.Init();
            //RepoMnt.Init();
        }
    }
}
