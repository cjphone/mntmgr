﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Microsoft.SolverFoundation.Common;
using Microsoft.SolverFoundation.Services;

using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Models;
using FPS.MNTMgr.Services.Schedule;

using FPS.MNTMgr.Repositories;
using FPS.MNTMgr.Services.DataAccess;
using FPS.MNTMgr.Web.Models;

using FPS.MNTMgr.Web.Controllers.Base;

namespace FPS.MNTMgr.Web.Controllers
{
    public class HomeController : MNTMgrDbController
    {
        public ActionResult Index()
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);
            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            DateTime baseInst = DateTime.Now.Date;

            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();
            viewModelMnt.MtrEngineers = site.MtrEngineers;

            viewModelMnt.MtrEvents = site.MtrEvents;

            return View(viewModelMnt);
        }

        public ActionResult Full()
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);
            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            viewModelMnt.ShouldDisplayFullCalendar = true;
            DateTime baseInst = DateTime.Now.Date;

            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();
            viewModelMnt.MtrEngineers = site.MtrEngineers;

            viewModelMnt.MtrEvents = site.MtrEvents;

            return View("Index", viewModelMnt);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Schedule()
        {

            DateTime baseInst = DateTime.Now.Date;

            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            MntCalculation calculation = new MntCalculation(site, site.MtrMntJobs.ToList(), site.MtrEngineers.Values.ToList());
            calculation.CompileSchedule(baseInst);
            calculation.CompileSchedule(baseInst.AddDays(1));
            calculation.CompileSchedule(baseInst.AddDays(2));


            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();
            viewModelMnt.MtrEngineers = site.MtrEngineers;

            viewModelMnt.MtrEvents = site.MtrEvents;

            return View("~/Views/Home/Index.cshtml", viewModelMnt);
        }

        /// <summary>
        /// reset, is really more for demo part
        /// </summary>
        /// <returns></returns>
        public ActionResult Reset()
        {

            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            if (site != null)
            {
                foreach (MtrMntJob mntJob in site.MtrMntJobs.Where(x => x.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished && x.ActualStatus != FPS.MNTMgr.Settings.AppSettings.MntJobState.Started))
                {
                    mntJob.ScheduledMtrEngineer = null;
                    mntJob.ScheduledMntStartInst = null;

                    mntJob.ActualMtrEngineer = null;
                    mntJob.ActualMntStartInst = null;
                    mntJob.ActualMntEndInst = null;

                    FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                    dataAccess.MtrMntJobsUpdate(site.Id, mntJob.Id, mntJob.DisplayName, mntJob.Description, mntJob.EarlyDueMntStartInst, mntJob.LateDueMntStartInst, null, null, null, null, null);

                }

                foreach (MtrEngineer mtrEngineer in site.MtrEngineers.Values)
                {
                    mtrEngineer.AssignedMntJobs = new Dictionary<DateTime, List<MtrMntJob>>();




                    //forth, to reset the working hours
                    for (int i = -10; i <= 30; i++)
                    {
                        DateTime workDay = DateTime.Now.Date.AddDays(i);

                        if (mtrEngineer.AvailableHours.ContainsKey(site.Id) && !mtrEngineer.AvailableHours[site.Id].ContainsKey(workDay))
                        {
                            double availableHours = 8;

                            if (workDay.DayOfWeek == DayOfWeek.Saturday || workDay.DayOfWeek == DayOfWeek.Sunday)
                            {
                                availableHours = 0;
                            }

                            FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                            dataAccess.MtrEngineerAvailHoursInsert(site.Id, mtrEngineer.Id, workDay, availableHours, "Default Value");
                        }
                    }

                }

                foreach (MtrMntJob mntJob in site.MtrMntJobs.Where(x => x.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished || x.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started || x.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled))
                {
                    MtrEngineer mtrEngineer = null;

                    if (mntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished && mntJob.ActualMtrEngineer != null && site.MtrEngineers.ContainsKey(mntJob.ActualMtrEngineer.Id))
                    {
                        mtrEngineer = site.MtrEngineers[mntJob.ActualMtrEngineer.Id];
                    }
                    else if (mntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started && mntJob.ActualMtrEngineer != null && site.MtrEngineers.ContainsKey(mntJob.ActualMtrEngineer.Id))
                    {
                        mtrEngineer = site.MtrEngineers[mntJob.ActualMtrEngineer.Id];
                    }
                    else if (mntJob.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled && mntJob.ScheduledMtrEngineer != null && site.MtrEngineers.ContainsKey(mntJob.ScheduledMtrEngineer.Id))
                    {
                        mtrEngineer = site.MtrEngineers[mntJob.ScheduledMtrEngineer.Id];
                    }

                    if (mtrEngineer != null)
                    {
                        if (!mtrEngineer.AssignedMntJobs.ContainsKey(mntJob.DisplayMntStart.Date))
                        {
                            mtrEngineer.AssignedMntJobs.Add(mntJob.DisplayMntStart.Date, new List<MtrMntJob>());
                        }

                        mtrEngineer.AssignedMntJobs[mntJob.DisplayMntStart.Date].Add(mntJob);


                    }
                }
            }
            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            DateTime baseInst = DateTime.Now.Date;

            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();

            viewModelMnt.MtrEngineers = site.MtrEngineers;
            viewModelMnt.MtrEvents = site.MtrEvents;

            return View("~/Views/Home/Index.cshtml", viewModelMnt);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }



        public JsonResult GetCalendar()
        {
            //var ApptListForDate = DiaryEvent.LoadAllAppointmentsInDateRange(start, end);
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            var jobs = from j in site.MtrMntJobs.OrderBy(x => x.ActualStatusSort).ThenBy(x => x.DisplayMntStart)
                       select new
                       {
                           id = j.Id,
                           title = j.DisplayName,
                           start = j.DisplayMntStart.ToString(AppSettings.FullCalendarDateTimeFormat),
                           end = j.DisplayMntStart.AddHours(j.MtrEvent.RequiredMntHours).ToString(AppSettings.FullCalendarDateTimeFormat),
                           color = j.ActualStatusColor,
                           AsgnEngineer = j.ScheduledMtrEngineer == null ? "Not Assigned" : j.ScheduledMtrEngineer.DisplayName,
                           Event = j.DisplayName,
                           Entity = j.MtrEquipment.DisplayName,
                           Description = j.Description,
                           allDay = false
                       };

            var rows = jobs.ToList();

            return Json(rows.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCalendarFull()
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            //var ApptListForDate = DiaryEvent.LoadAllAppointmentsInDateRange(start, end);
            var jobs = from j in site.MtrMntJobs.OrderBy(x => x.ActualStatusSort).ThenBy(x => x.DisplayMntStart)
                       select new
                       {
                           id = j.Id,
                           title = j.DisplayName,
                           start = j.DisplayMntStart.ToString(AppSettings.FullCalendarDateTimeFormat),
                           end = j.DisplayMntStart.AddHours(j.MtrEvent.RequiredMntHours).ToString(AppSettings.FullCalendarDateTimeFormat),
                           color = j.ActualStatusColor,
                           AsgnEngineer = j.ScheduledMtrEngineer == null ? "Not Assigned" : j.ScheduledMtrEngineer.DisplayName,
                           Event = j.DisplayName,
                           Entity = j.MtrEquipment.DisplayName,
                           Description = j.Description,
                           allDay = false
                       };

            var rows = jobs.ToList();

            foreach (MtrEngineer mtrEngineer in site.MtrEngineers.Values.Where(x => x.AvailableHours.ContainsKey(site.Id)))
            {
                foreach (KeyValuePair<DateTime, double> kv in mtrEngineer.AvailableHours[site.Id].Where(x => x.Value > 0))
                {
                    string engineerHourColor = "#3366ff";
                    string engineerHourMsg = mtrEngineer.DisplayName + " Avail: " + kv.Value + "(h)";
                    if (kv.Value == 0)
                    {
                        engineerHourColor = "#aaaaaa";
                        engineerHourMsg = "Not Available";
                    }
                    var job = new
                    {
                        id = "-",
                        title = engineerHourMsg,
                        start = kv.Key.ToString(AppSettings.FullCalendarDateTimeFormat),
                        end = kv.Key.AddHours(24).ToString(AppSettings.FullCalendarDateTimeFormat),
                        color = engineerHourColor,
                        AsgnEngineer = "",
                        Event = "",
                        Entity = "",
                        Description = "",
                        allDay = true
                    };
                    rows.Add(job);

                }
            }


            return Json(rows.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCalendarMtrEngineer(string id)
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            //var ApptListForDate = DiaryEvent.LoadAllAppointmentsInDateRange(start, end);
            var jobs = from j in site.MtrMntJobs.Where(x => x.ScheduledMtrEngineer != null && x.ScheduledMtrEngineer.Id == id).OrderBy(x => x.ActualStatusSort).ThenBy(x => x.DisplayMntStart)
                       select new
                       {
                           id = j.Id,
                           title = j.DisplayName,
                           start = j.DisplayMntStart.ToString(AppSettings.FullCalendarDateTimeFormat),
                           end = j.DisplayMntStart.AddHours(j.MtrEvent.RequiredMntHours).ToString(AppSettings.FullCalendarDateTimeFormat),
                           color = j.ActualStatusColor,
                           AsgnEngineer = j.ScheduledMtrEngineer == null ? "Not Assigned" : j.ScheduledMtrEngineer.DisplayName,
                           Event = j.DisplayName,
                           Entity = j.MtrEquipment.DisplayName,
                           Description = j.Description,
                           allDay = false
                       };

            var rows = jobs.ToList();

            if (site.MtrEngineers.ContainsKey(id) && site.MtrEngineers[id].AvailableHours.ContainsKey(site.Id))
            {
                foreach (KeyValuePair<DateTime, double> kv in site.MtrEngineers[id].AvailableHours[site.Id])
                {
                    string engineerHourColor = "#3366ff";
                    string engineerHourMsg = "Avail: " + kv.Value + "(h)";
                    if (kv.Value == 0)
                    {
                        engineerHourColor = "#aaaaaa";
                        engineerHourMsg = "Not Available";
                    }
                    var job = new
                    {
                        id = "-",
                        title = engineerHourMsg,
                        start = kv.Key.ToString(AppSettings.FullCalendarDateTimeFormat),
                        end = kv.Key.AddHours(24).ToString(AppSettings.FullCalendarDateTimeFormat),
                        color = engineerHourColor,
                        AsgnEngineer = site.MtrEngineers[id].DisplayName,
                        Event = "",
                        Entity = "",
                        Description = "",
                        allDay = true
                    };
                    rows.Add(job);

                }
            }


            return Json(rows.ToArray(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCalendarMtrEquipment(string id)
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);

            //var ApptListForDate = DiaryEvent.LoadAllAppointmentsInDateRange(start, end);
            var jobs = from j in site.MtrMntJobs.Where(x => x.MtrEquipment != null && x.MtrEquipment.Id == id).OrderBy(x => x.ActualStatusSort).ThenBy(x => x.DisplayMntStart)
                       select new
                       {
                           id = j.Id,
                           title = j.DisplayName,
                           start = j.DisplayMntStart.ToString(AppSettings.FullCalendarDateTimeFormat),
                           end = j.DisplayMntStart.AddHours(j.MtrEvent.RequiredMntHours).ToString(AppSettings.FullCalendarDateTimeFormat),
                           color = j.ActualStatusColor,
                           AsgnEngineer = j.ScheduledMtrEngineer == null ? "Not Assigned" : j.ScheduledMtrEngineer.DisplayName,
                           Event = j.DisplayName,
                           Entity = j.MtrEquipment.DisplayName,
                           Description = j.Description,
                           allDay = false
                       };

            var rows = jobs.ToList();


            return Json(rows.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Engineer(string id)
        {


            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);
            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            DateTime baseInst = DateTime.Now.Date;

            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();
            viewModelMnt.MtrEngineers = site.MtrEngineers;
            viewModelMnt.MtrEvents = site.MtrEvents;


            if (!string.IsNullOrEmpty(id) && site.MtrEngineers.ContainsKey(id))
            {
                viewModelMnt.SelectedMtrEngineer = site.MtrEngineers[id];

            }

            return View("Index", viewModelMnt);


        }

        public ActionResult Equipment(string id)
        {


            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == AppSettings.DefaultSiteId);
            ViewModelMnt viewModelMnt = new ViewModelMnt(site);
            DateTime baseInst = DateTime.Now.Date;

            viewModelMnt.TodayMntJobs = site.MtrMntJobs.Where(x => x.DisplayMntStart < baseInst.AddDays(1) || x.LateDueMntStartInst < baseInst.AddDays(1)).OrderBy(x => x.DisplayMntStart).ThenBy(x => x.ActualStatusSort).ToList();
            viewModelMnt.MtrEngineers = site.MtrEngineers;
            viewModelMnt.MtrEvents = site.MtrEvents;

            if (!string.IsNullOrEmpty(id) && site.MtrEquipments.ContainsKey(id))
            {
                viewModelMnt.SelectedMtrEquipment = site.MtrEquipments[id];

            }

            return View("Index", viewModelMnt);

        }
    }
}