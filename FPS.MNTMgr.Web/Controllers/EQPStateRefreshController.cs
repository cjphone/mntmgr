﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;


namespace FPS.MNTMgr.Web.Controllers
{
    public class EQPStateRefreshController : Controller
    {
        // GET: EQPStateRefresh
        public ActionResult Index()
        {

            FPS.MNTMgr.Web.SignalR.EqpStateRefresh refresh = new SignalR.EqpStateRefresh();

            Task loadMntMgr = Task.Factory.StartNew(() => { refresh.LoadMntMgr(); });
            Task loadBayView = Task.Factory.StartNew(() => { refresh.LoadBayView(); });

            return View();
        }
    }
}