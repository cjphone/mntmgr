﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPS.MNTMgr.Web.Controllers.Base
{
    public class FPSDWHDbController : BaseCultureController
    {
        protected ConnectionStringSettings defaultConnectionStringSettings;

        public FPSDWHDbController()
        {
            defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["FPSDWH"];
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }
}