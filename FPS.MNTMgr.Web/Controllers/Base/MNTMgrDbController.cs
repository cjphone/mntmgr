﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPS.MNTMgr.Web.Controllers.Base
{
    public class MNTMgrDbController : BaseCultureController
    {
        protected ConnectionStringSettings defaultConnectionStringSettings;

        public MNTMgrDbController()
        {
            defaultConnectionStringSettings = ConfigurationManager.ConnectionStrings["MNTMGRDWH"];
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }
}