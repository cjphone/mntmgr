﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Microsoft.SolverFoundation.Common;
using Microsoft.SolverFoundation.Services;

using FPS.MNTMgr.Settings;
using FPS.MNTMgr.Models;
using FPS.MNTMgr.Services.Schedule;

using FPS.MNTMgr.Repositories;
using FPS.MNTMgr.Services.DataAccess;
using FPS.MNTMgr.Web.Models;

using FPS.MNTMgr.Web.Controllers.Base;

namespace FPS.MNTMgr.Web.Controllers
{
    public class ProcessFamilyViewController : FPSDWHDbController
    {
        // GET: BayView
        public ActionResult Index()
        {


            ViewModelSummaryView viewModelSummaryView = new ViewModelSummaryView();

            viewModelSummaryView.ProcessFamilies = SummaryViewRepository.ProcessFamilies;

            return View(viewModelSummaryView);

        }
    }
}