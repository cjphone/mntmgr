﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using System.Threading;

using FPS.Applications.Extensions;

using FPS.MNTMgr.Web.Controllers.Base;
using FPS.MNTMgr.Web.Models;
using FPS.MNTMgr.Languages.Helpers;
using FPS.MNTMgr.Repositories;
using FPS.MNTMgr.Models;

namespace FPS.MNTMgr.Web.Controllers
{
    public class CommonController : MNTMgrDbController
    {
        public string SetCulture(string culture, string calendarlocalfile)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);

            RouteData.Values["culture"] = culture;

            // Save culture in a cookie
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureCookie.Value = culture;   // update cookie value
            else
            {

                cultureCookie = new HttpCookie("_culture");
                cultureCookie.Value = culture;
                cultureCookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cultureCookie);

            // Save calendar local file in a cookie
            HttpCookie calendarlocalfileCookie = Request.Cookies["_calendarlocalfile"];
            if (calendarlocalfileCookie != null)
                calendarlocalfileCookie.Value = calendarlocalfile;   // update cookie value
            else
            {

                calendarlocalfileCookie = new HttpCookie("_calendarlocalfile");
                calendarlocalfileCookie.Value = calendarlocalfile;
                calendarlocalfileCookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(calendarlocalfileCookie);

            // Modify current thread's cultures           
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            CultureInfo.DefaultThreadCurrentCulture = Thread.CurrentThread.CurrentCulture;
            CultureInfo.DefaultThreadCurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return Resources.GetText(FPS.MNTMgr.Settings.AppSettings.Message.Success);
        }

        /// <summary>
        /// adding add a new area
        /// </summary>
        /// <param name="siteAreaId"></param>
        /// <param name="siteAreaDisplayName"></param>
        /// <param name="siteAreaDescription"></param>
        /// <returns></returns>
        public string AddNewSiteArea(string siteAreaId, string siteAreaDisplayName, string siteAreaDescription)
        {
            siteAreaId = siteAreaId.RemoveSpecialCharacters();

            if (!string.IsNullOrEmpty(siteAreaId))
            {

                if (string.IsNullOrEmpty(siteAreaDisplayName))
                {
                    siteAreaDisplayName = siteAreaId;
                }

                if (string.IsNullOrEmpty(siteAreaDescription))
                {
                    siteAreaDescription = siteAreaId;
                }

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                return dataAccess.MtrSiteAreasInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, siteAreaId, siteAreaDisplayName, siteAreaDescription);
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        /// <summary>
        /// add a new skill
        /// </summary>
        /// <param name="mtrSkillId"></param>
        /// <param name="mtrSkillDisplayName"></param>
        /// <param name="mtrSkillDescription"></param>
        /// <returns></returns>
        public string AddNewMtrSkill(string mtrSkillId, string mtrSkillDisplayName, string mtrSkillDescription)
        {
            if (!string.IsNullOrEmpty(mtrSkillId))
            {

                if (string.IsNullOrEmpty(mtrSkillDisplayName))
                {
                    mtrSkillDisplayName = mtrSkillId;
                }

                if (string.IsNullOrEmpty(mtrSkillDescription))
                {
                    mtrSkillDescription = mtrSkillId;
                }

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                return dataAccess.MtrSkillsInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, mtrSkillId, mtrSkillDisplayName, mtrSkillDescription);
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        /// <summary>
        /// add a new event
        /// </summary>
        /// <param name="mtrEventId"></param>
        /// <param name="mtrEventRequiredHours"></param>
        /// <param name="mtrEventDisplayName"></param>
        /// <param name="mtrEventDescription"></param>
        /// <param name="mtrEventRequiredSkills"></param>
        /// <returns></returns>
        public string AddNewMtrEvent(string mtrEventId, string mtrEventRequiredHours, string mtrEventDisplayName, string mtrEventDescription, string mtrEventRequiredSkills)
        {
            mtrEventId = mtrEventId.RemoveSpecialCharacters();

            if (!string.IsNullOrEmpty(mtrEventId) && !string.IsNullOrEmpty(mtrEventRequiredSkills))
            {

                if (string.IsNullOrEmpty(mtrEventDisplayName))
                {
                    mtrEventDisplayName = mtrEventId;
                }

                if (string.IsNullOrEmpty(mtrEventDescription))
                {
                    mtrEventDescription = mtrEventId;
                }

                if (string.IsNullOrEmpty(mtrEventRequiredHours))
                {
                    mtrEventRequiredHours = "4";
                }

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                dataAccess.MtrEventsInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, mtrEventId, mtrEventRequiredHours, mtrEventDisplayName, mtrEventDescription);


                List<string> skills = mtrEventRequiredSkills.Split(',').ToList();

                foreach (string skill in skills)
                {
                    if (!string.IsNullOrEmpty(skill.Trim()))
                    {
                        dataAccess.MtrEventSkillsInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, mtrEventId, skill.Trim(), skill.Trim() + " on " + mtrEventId);
                    }
                }


                return FPS.MNTMgr.Settings.AppSettings.Message.Success;
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        /// <summary>
        /// adding a new resource
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string AddNewSiteResource(string culture, string name, string value)
        {
            if (!string.IsNullOrEmpty(culture) && !string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
            {
                FPS.MNTMgr.Languages.Services.DataAccess.CommDataAccess dataAccess = new Languages.Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                return dataAccess.AddLanguageSiteResource(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, culture, name, value);
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        public string AddNewEngineer(string userId, string fullName, string displayName, string description, string skills)
        {
            userId = userId.RemoveSpecialCharacters();

            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(displayName))
            {

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                //first to add engineer
                dataAccess.MtrEngineersInsert(userId, fullName, displayName, description);

                //second to add site
                Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);
                dataAccess.MtrSiteEngineersInsert(site.Id, userId, "A", "Default Value");

                //thrid to add skills, for now, default to all possible skills
                List<string> skillList = skills.Split(',').ToList();
                foreach (string skill in skillList)
                {
                    if (!string.IsNullOrEmpty(skill.Trim()))
                    {
                        dataAccess.MtrEngineerSkillsInsert(site.Id, userId, skill.Trim(), 10, "Default Value");
                    }
                }

                //forth, to reset the working hours
                for (int i = -10; i <= 30; i++)
                {
                    DateTime workDay = DateTime.Now.Date.AddDays(i);
                    double availableHours = 8;

                    if (workDay.DayOfWeek == DayOfWeek.Saturday || workDay.DayOfWeek == DayOfWeek.Sunday)
                    {
                        availableHours = 0;
                    }

                    dataAccess.MtrEngineerAvailHoursInsert(site.Id, userId, workDay, availableHours, "Default Value");
                }

                return FPS.MNTMgr.Settings.AppSettings.Message.Success;
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        public string AddNewEquipment(string EquipmentId, string EquipmentType, string MainEquipmentId, string EquipmentArea, string EquipmentDisplayName, string EquipmentDescription)
        {
            EquipmentId = EquipmentId.RemoveSpecialCharacters();

            if (!string.IsNullOrEmpty(EquipmentId) && !string.IsNullOrEmpty(EquipmentType) && !string.IsNullOrEmpty(EquipmentArea))
            {
                if (string.IsNullOrEmpty(MainEquipmentId))
                {
                    MainEquipmentId = EquipmentId;
                }

                if (string.IsNullOrEmpty(EquipmentDisplayName))
                {
                    EquipmentDisplayName = EquipmentId;
                }

                if (string.IsNullOrEmpty(EquipmentDescription))
                {
                    EquipmentDescription = EquipmentId;
                }

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                return dataAccess.MtrEquipmentsInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, EquipmentArea, EquipmentId, MainEquipmentId, EquipmentType, EquipmentDisplayName, EquipmentDescription);
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }


        public string AddNewMntJob(string MntJobTitle, string MtrEquipmentId, string MtrEventId, string MntJobDescription, string MntEarlyDueDate, string MntLateDueDate)
        {
            if (!string.IsNullOrEmpty(MntJobTitle) && !string.IsNullOrEmpty(MtrEquipmentId) && !string.IsNullOrEmpty(MtrEventId) && !string.IsNullOrEmpty(MntEarlyDueDate) && !string.IsNullOrEmpty(MntLateDueDate))
            {
                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                string jobId = MntJobTitle + "on" + MtrEquipmentId + "for" + MtrEventId + "dueon" + MntLateDueDate;

                jobId = jobId.RemoveSpecialCharacters().ToLower();

                if (jobId.Length > 120)
                {
                    jobId = jobId.Substring(0, 120);
                }

                return dataAccess.MtrMntJobsInsert(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, jobId, MtrEquipmentId, MtrEventId, MntJobTitle, MntJobDescription, MntEarlyDueDate, MntLateDueDate);
            }
            else
            {
                return "Insufficient data. Please verify.";
            }
        }

        /// <summary>
        /// this is to update the event with the new start and end time
        /// </summary>
        /// <param name="id"></param>
        /// <param name="neweventstart"></param>
        /// <param name="neweventend"></param>
        /// <returns></returns>
        public string UpdateMntJob(string id, string neweventstart, string neweventend)
        {
            Site site = SiteRepository.Sites.Values.FirstOrDefault(x => x.Id == FPS.MNTMgr.Settings.AppSettings.DefaultSiteId);

            DateTime newStart = DateTime.Parse(neweventstart);
            DateTime newEnd = DateTime.Parse(neweventend);

            string scheduledEngineerId = string.Empty;
            string actualEngineerId = string.Empty;

            if (!string.IsNullOrEmpty(id) && site.MtrMntJobs.Any(x => x.Id == id))
            {
                MtrMntJob job = site.MtrMntJobs.FirstOrDefault(x => x.Id == id);
                //1.Due
                /// 2. Pending
                /// 3. Started
                /// 4. Scheduled
                /// 5. Finished
                if (job.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Finished)
                {
                    //do nothing
                    return "Shouldn't adjusted the schedule of finished job";
                }
                else if (job.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Scheduled)
                {
                    job.ScheduledMntStartInst = newStart;


                }
                else if (job.ActualStatus == FPS.MNTMgr.Settings.AppSettings.MntJobState.Started)
                {
                    job.ActualMntStartInst = newStart;
                    job.ActualMntEndInst = newEnd;
                }
                else
                {
                    job.EarlyDueMntStartInst = new DateTime(newStart.Year, newStart.Month, newStart.Day);
                    job.LateDueMntStartInst = new DateTime(newStart.Year, newStart.Month, newStart.Day);
                }

                if (job.ScheduledMtrEngineer != null)
                {
                    scheduledEngineerId = job.ScheduledMtrEngineer.Id;
                }

                if (job.ActualMtrEngineer != null)
                {
                    actualEngineerId = job.ActualMtrEngineer.Id;
                }

                FPS.MNTMgr.Services.DataAccess.CommDataAccess dataAccess = new Services.DataAccess.CommDataAccess(defaultConnectionStringSettings);

                return dataAccess.MtrMntJobsUpdate(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, job.Id, job.DisplayName, job.Description, job.EarlyDueMntStartInst, job.LateDueMntStartInst, scheduledEngineerId, job.ScheduledMntStartInst, actualEngineerId, job.ActualMntStartInst, job.ActualMntEndInst);

            }

            return FPS.MNTMgr.Settings.AppSettings.Message.Success;
        }
    }
}