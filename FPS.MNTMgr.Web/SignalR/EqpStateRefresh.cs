﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

using FPS.MNTMgr.Models;
using FPS.MNTMgr.Repositories;

namespace FPS.MNTMgr.Web.SignalR
{
    public class EqpStateRefresh : Hub
    {
        public void LoadMntMgr()
        {

            bool IsRefreshing = true;

            int i = 1;

            while (IsRefreshing)
            {

                foreach (Site site in FPS.MNTMgr.Repositories.SiteRepository.Sites.Values)
                {
                    foreach (MtrEquipment eqp in site.MtrEquipments.Values)
                    {
                        i++;
                        eqp.CurrMtrAttentions = new List<MtrAttention>();

                        if (i % 5 == 0)
                        {
                            eqp.CurrStateDisplay = "UDT";
                            eqp.CurrStateStartInst = DateTime.Now;
                            eqp.CurrStateDisplayClassName = "eqp_state_udt";


                            MtrAttention attention = new MtrAttention(eqp, eqp.CurrStateStartInst)
                            {
                                CurrPrioiryDisplay = "Urgent",
                                CurrPrioirySort = 1
                            };

                            eqp.CurrMtrAttentions.Add(attention);
                        }
                        else if (i % 5 == 1)
                        {
                            eqp.CurrStateDisplay = "SDT";
                            eqp.CurrStateStartInst = DateTime.Now;
                            eqp.CurrStateDisplayClassName = "eqp_state_sdt";
                        }
                        else if (i % 5 == 2)
                        {
                            eqp.CurrStateDisplay = "SBY";
                            eqp.CurrStateStartInst = DateTime.Now;
                            eqp.CurrStateDisplayClassName = "eqp_state_sby";
                        }
                        else if (i % 5 == 3)
                        {
                            eqp.CurrStateDisplay = "PRD";
                            eqp.CurrStateStartInst = DateTime.Now;
                            eqp.CurrStateDisplayClassName = "eqp_state_prd";
                        }
                        else if (i % 5 == 4)
                        {
                            eqp.CurrStateDisplay = "ENG";
                            eqp.CurrStateStartInst = DateTime.Now;
                            eqp.CurrStateDisplayClassName = "eqp_state_eng";
                        }


                        var context = GlobalHost.ConnectionManager.GetHubContext<EqpStateRefresh>();
                        context.Clients.All.refreshEqpState(eqp.Id, eqp.CurrStateDisplay, eqp.CurrStateDisplayClassName);

                        //Clients.All.refreshEqpState(eqp.Id, eqp.CurrStateDisplay, eqp.CurrStateDisplayClassName);

                        System.Threading.Thread.Sleep(1000 * 60);
                    }
                }
            }

        }

        public void LoadBayView()
        {

            bool IsRefreshing = true;

            while (IsRefreshing)
            {
                SummaryViewRepository.Init();

                foreach (Bay bay in SummaryViewRepository.Bays.Values)
                {
                    foreach (Tool tool in bay.Tools.Values)
                    {



                        var context = GlobalHost.ConnectionManager.GetHubContext<EqpStateRefresh>();
                        context.Clients.All.refreshBayView(tool.Id, tool.StateDisplay, tool.WebStyle.WebStyleText);

                        //Clients.All.refreshEqpState(eqp.Id, eqp.CurrStateDisplay, eqp.CurrStateDisplayClassName);


                    }
                }


                var refreshSummary = GlobalHost.ConnectionManager.GetHubContext<EqpStateRefresh>();
                refreshSummary.Clients.All.refreshSummarySection(DateTime.Now.ToString(), SummaryViewRepository.Bays.Sum(x => x.Value.NumToolsUp), SummaryViewRepository.Bays.Sum(x => x.Value.NumToolsDown), SummaryViewRepository.Bays.Sum(x => x.Value.NumToolsNST));

                System.Threading.Thread.Sleep(1000 * 60);
            }

        }
    }
}