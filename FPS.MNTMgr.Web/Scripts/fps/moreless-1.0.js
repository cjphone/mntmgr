﻿function showMore(e) {
    DisableAutoRefresh();
    var parentItem = $(e).parents(".moreLessParent").first();
    parentItem.find(".moreLessObject").removeClass("hidden");
    parentItem.find(".moreButton").addClass("hidden");
    parentItem.find(".lessButton").removeClass("hidden");
    parentItem.find(".viewMode").val("Opened");
}

function showLess(e) {
    EnableAutoRefresh();
    var parentItem = $(e).parents(".moreLessParent").first();
    parentItem.find(".moreLessObject").addClass("hidden");
    parentItem.find(".moreButton").removeClass("hidden");
    parentItem.find(".lessButton").addClass("hidden");
    parentItem.find(".viewMode").val("Closed");
}