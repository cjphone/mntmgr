﻿function EnableAutoRefresh() {
    //enable auto refresh
    window.autoRefreshEnabled = true;
    //show enable buttons
    $(".autorefresh-button-enabled").show();
    //not show disable buttons
    $(".autorefresh-button-disabled").hide();
}

function DisableAutoRefresh() {
    //disable auto refresh
    window.autoRefreshEnabled = false;
    //not show enable buttons
    $(".autorefresh-button-enabled").hide();
    //show disable buttons
    $(".autorefresh-button-disabled").show();
}