﻿
$(function () {
    $("#popup-box").dialog({
        autoOpen: false,
        height: 600,
        width: 800,
        modal: true,
        buttons: {
            "Close": function () {
                $(this).dialog("close");
                $(this).find(".body").empty();
            }
        }
    });
});

function showPopupContent(url, popupTitle, width, height, dataToPass) {

    //DisableAutoRefresh();

    if (popupTitle !== null) {
        //set title
        $("#popup-box").dialog("option", "title", popupTitle);
    }

    if (width !== null) {
        //set width
        $("#popup-box").dialog("option", "width", width);
    }

    if (height !== null) {
        //set height
        $("#popup-box").dialog("option", "height", height);
    }

    //Set data to empty string if not passed in
    dataToPass = dataToPass || "";

    //open the lot list box
    $("#popup-box").dialog("open");

    //show the loading pictures
    $("#popup-box .loading").show();

    //clean the content
    $("#popup-box .body").empty();

    // Send the data using post
    var get = $.get(url)
        .done(function (data) {

            $("#popup-box .loading").hide();

            $("#popup-box .body").html(data);
        })
        .fail(function (data) {
            $("#popup-box .loading").hide();

            $("#popup-box .body").html(data);
        });
}



function showPopupLotDetails(url, width, height) {

    //DisableAutoRefresh();

    if (url.length > 1) {

        //window.popupHoverMsgIndex += 1;

        var index = window.popupHoverMsgIndex++;

        //alert(window.pageYOffset);

        $("<div id='popup-box-" + index + "' style='border: solid 2px #999999; background-color:#ffffff;' title='Details'><div class='body'>Loading......</div></div>").insertBefore($("#popup-box"));

        $("#popup-box-" + index).dialog({
            autoOpen: true,
            height: height,
            width: width,
            modal: false,
            /*
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                    $(this).find(".body").empty();
                }
            },
            */
            close: function (event, ui) {
                //remove content
                $("#popup-box-" + index + " .body").html("");
                $("#popup-box-" + index).hide();
            }
        });

        var get = $.get(url)
        .done(function (data) {
            $("#popup-box-" + index + " .body").html(data);
        })
        .fail(function (data) {
            $("#popup-box-" + index + " .body").html(data);
        });
    }
}


