﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models;

namespace FPS.MNTMgr.Web.Models
{
    public class ViewModelMnt
    {
        /// <summary>
        /// the site for this view Model
        /// </summary>
        public Site Site { get; set; }

        /// <summary>
        /// the list of Mnt Jobs of Day
        /// </summary>
        public List<MtrMntJob> TodayMntJobs { get; set; }

        /// <summary>
        /// the list of engineers
        /// </summary>
        public Dictionary<string, MtrEngineer> MtrEngineers { get; set; }

        /// <summary>
        /// the list of events
        /// </summary>
        public Dictionary<string, MtrEvent> MtrEvents { get; set; }

        /// <summary>
        /// indicate if it will display full calendar, including engineers' availabilities
        /// </summary>
        public bool ShouldDisplayFullCalendar { get; set; }

        /// <summary>
        /// selected Mtr Engineer
        /// </summary>
        public MtrEngineer SelectedMtrEngineer { get; set; }

        /// <summary>
        /// selected Mtr Equipment
        /// </summary>
        public MtrEquipment SelectedMtrEquipment { get; set; }

        /// <summary>
        /// the view model
        /// </summary>
        public ViewModelMnt(Site site)
        {
            Site = site;
            TodayMntJobs = new List<MtrMntJob>();
            MtrEngineers = new Dictionary<string, MtrEngineer>();
            MtrEvents = new Dictionary<string, MtrEvent>();
            SelectedMtrEngineer = null;
            SelectedMtrEquipment = null;
            ShouldDisplayFullCalendar = false;
        }
    }
}