﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FPS.MNTMgr.Models;

namespace FPS.MNTMgr.Web.Models
{
    public class ViewModelSummaryView
    {
        public Dictionary<string, Bay> Bays { get; set; }
        public Dictionary<string, ProcessFamily> ProcessFamilies { get; set; }

        public int NumToolsUp => Bays.Values.Sum(x => x.NumToolsUp);
        public int NumToolsDown => Bays.Values.Sum(x => x.NumToolsDown);
        public int NumToolsNST => Bays.Values.Sum(x => x.NumToolsNST);

        public ViewModelSummaryView()
        {
            Bays = new Dictionary<string, Bay>();
            ProcessFamilies = new Dictionary<string, ProcessFamily>();
        }
    }
}