﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Threading;


namespace FPS.MNTMgr.Web.Models
{
    public static class Resources
    {

        public static string GetText( string name)
        {

            return FPS.MNTMgr.Languages.Models.Resources.GetText(FPS.MNTMgr.Settings.AppSettings.DefaultSiteId, name, CultureInfo.CurrentUICulture.Name);


        }

    }
}